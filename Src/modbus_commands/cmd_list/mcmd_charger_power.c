#include "modbus_commands/cmd_list/mcmd_charger_power.h"
#include "modbus_commands/mcmd_values.h"
#include "modbus_datamodel/modbus_datamodel_addr.h"

#include "initialize_peripherials/initialize_peripherials.h"

#include <stdbool.h>


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void mcmd_charger_power_initialize_callback(modbus_cmd_t *cmd);

static void mcmd_charger_power_handler_callback(modbus_cmd_t *cmd);

static bool is_new_cmd_received(modbus_cmd_t *cmd);

static bool is_cmd_equal_to(modbus_cmd_t *cmd, uint16_t cmd_val);

static void charger_power_enable_handler(modbus_cmd_t *cmd);

static void charger_power_disable_handler(modbus_cmd_t *cmd);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

modbus_cmd_t mcmd_charger_power =
{
    .cmd_val = 0,
    .cmd_resp = 0,
    .init_clbk = mcmd_charger_power_initialize_callback,
    .handler_clbk = mcmd_charger_power_handler_callback
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void mcmd_charger_power_initialize_callback(modbus_cmd_t *cmd)
{
    cmd->cmd_val = &mc->hr_a[MHR_CMD_CHARG_POWER];
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void mcmd_charger_power_handler_callback(modbus_cmd_t *cmd)
{
    if(is_new_cmd_received(cmd) == true)
    {
    	charger_power_enable_handler(cmd);
    	charger_power_disable_handler(cmd);
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_new_cmd_received(modbus_cmd_t *cmd)
{
    return (*cmd->cmd_val != cmd->cmd_resp);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_cmd_equal_to(modbus_cmd_t *cmd, uint16_t cmd_val)
{
	return (*cmd->cmd_val == cmd_val);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void charger_power_enable_handler(modbus_cmd_t *cmd)
{
    if(is_cmd_equal_to(cmd, MCMD_CHRG_PWR_ENABLE) == true)
    {
    	plc_module_set_single_output(plc[CHARGER_POWER_RELAY_PLC_ID],
    								 CHARGER_POWER_RELAY_PLC_OUT,
									 ON);

    	if(plc_module_get_output_state(plc[CHARGER_POWER_RELAY_PLC_ID],
    								   CHARGER_POWER_RELAY_PLC_OUT) == ON)
    	{
    		cmd->cmd_resp = MCMD_CHRG_PWR_ENABLE;
    	}
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void charger_power_disable_handler(modbus_cmd_t *cmd)
{
	if(is_cmd_equal_to(cmd, MCMD_CHRG_PWR_DISABLE) == true)
	{
    	plc_module_set_single_output(plc[CHARGER_POWER_RELAY_PLC_ID],
    								 CHARGER_POWER_RELAY_PLC_OUT,
									 OFF);

    	if(plc_module_get_output_state(plc[CHARGER_POWER_RELAY_PLC_ID],
    								   CHARGER_POWER_RELAY_PLC_OUT) == OFF)
    	{
    		cmd->cmd_resp = MCMD_CHRG_PWR_DISABLE;
    	}
	}
}
