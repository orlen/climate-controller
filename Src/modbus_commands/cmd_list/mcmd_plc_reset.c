#include "modbus_commands/cmd_list/mcmd_plc_reset.h"
#include "modbus_commands/mcmd_values.h"
#include "modbus_datamodel/modbus_datamodel_addr.h"

#include "initialize_peripherials/initialize_peripherials.h"

#include <stdbool.h>


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void mcmd_plc_reset_initialize_callback(modbus_cmd_t *cmd);

static void mcmd_plc_reset_handler_callback(modbus_cmd_t *cmd);

static bool is_new_cmd_received(modbus_cmd_t *cmd);

static void plc_reset_handler(modbus_cmd_t *cmd);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

modbus_cmd_t mcmd_plc_reset =
{
    .cmd_val = 0,
    .cmd_resp = 0,
    .init_clbk = mcmd_plc_reset_initialize_callback,
    .handler_clbk = mcmd_plc_reset_handler_callback
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void mcmd_plc_reset_initialize_callback(modbus_cmd_t *cmd)
{
    cmd->cmd_val = &mc->hr_a[MHR_CMD_PLC_RESET];
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void mcmd_plc_reset_handler_callback(modbus_cmd_t *cmd)
{
    if(is_new_cmd_received(cmd) == true)
    {
    	plc_reset_handler(cmd);
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_new_cmd_received(modbus_cmd_t *cmd)
{
    return (*cmd->cmd_val != cmd->cmd_resp);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void plc_reset_handler(modbus_cmd_t *cmd)
{
    if(*cmd->cmd_val == MCMD_PLC_RESET)
    {
        plc_module_reset_module(plc[PLC1_ID]);
        *cmd->cmd_val = 0x0000;
    }
}
