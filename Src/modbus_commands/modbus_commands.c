#include "modbus_commands/modbus_commands.h"
#include "modbus_commands/modbus_cmd.h"
#include "modbus/modbus_port.h"
#include <stdbool.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void initialize_commands(void);

static void execute_commands(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool init_flag = 0;

static modbus_cmd_t *commands_list[] = 
{
	&mcmd_charger_logic,
	&mcmd_charger_power,
	&mcmd_irlock,
	&mcmd_plc_reset
};

static uint8_t cmd_cnt = 4;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_commands_handler(void)
{
    initialize_commands();
    execute_commands();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void initialize_commands(void)
{
    if(init_flag == false)
    {
        for(int cmd_id = 0; cmd_id < cmd_cnt; cmd_id++)
        {
            modbus_cmd_t *p_mcmd = commands_list[cmd_id];
            p_mcmd->init_clbk(p_mcmd);
        }
        init_flag = true;
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void execute_commands(void)
{
    if(init_flag == true)
    {
        for(int cmd_id = 0; cmd_id < cmd_cnt; cmd_id++)
        {
            modbus_cmd_t *p_mcmd = commands_list[cmd_id];
            p_mcmd->handler_clbk(p_mcmd);
        }
    }
}
