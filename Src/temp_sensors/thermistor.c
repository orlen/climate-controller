#include "temp_sensors/thermistor.h"

#include <stdlib.h>
#include <math.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static double calculate_resistance(thermistor_t *th);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

thermistor_t *thermistor_create()
{
    return (thermistor_t*)malloc(sizeof(thermistor_t));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

double thermistor_get_temp_K(thermistor_t *th)
{
    double resistance = calculate_resistance(th);

    return (th->beta_value * th->nom_temperature)
    		/ (th->beta_value + (th->nom_temperature
            * log(resistance / th->nom_resistance)));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

double thermistor_get_temp_C(thermistor_t *th)
{
    return thermistor_get_temp_K(th) - 273.15;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static double calculate_resistance(thermistor_t *th)
{
    return th->ref_resistance / (th->adc_res / (*th->adc_val) - 1);
}
