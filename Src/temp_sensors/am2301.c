#include "temp_sensors/am2301.h"

#include "debug.h"
#include <stdlib.h>
#include <stdbool.h>


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define AM2301_DEBUG

#define AM2301_OUT_HIGH(am)  (am->gpiox->BSRR = (uint32_t)am->gpio_pin_x)
#define AM2301_OUT_LOW(am)   (am->gpiox->BSRR = (uint32_t)am->gpio_pin_x<<16)

#define AM2301_IN_READ(am)   (am->gpiox->IDR & (uint32_t)am->gpio_pin_x)

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static GPIO_InitTypeDef gpio_am2301;

static void data_pin_output(am2301_t *am);

static void data_pin_input(am2301_t *am);

static bool is_parity_data_correct(uint8_t *data);

static void get_temp_and_hum_from_data(am2301_t *am, uint8_t *data);

static void delay_asm(uint32_t us);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

am2301_t *am2301_create()
{
    return (am2301_t*)malloc(sizeof(am2301_t));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

int am2301_read_data(am2301_t *am)
{
    uint16_t tim = 0;
    uint8_t data[5];

    data_pin_output(am);
    AM2301_OUT_LOW(am);
    delay_asm(6000);
    AM2301_OUT_HIGH(am);
    delay_asm(25);
    data_pin_input(am);

    while(!AM2301_IN_READ(am))
    {
    	if(tim > 56)
    	{
    		return am2301_comm_error;
    	}
    	tim++;
    }

    while(AM2301_IN_READ(am))
    {
    	if(tim > 56)
    	{
    		return am2301_comm_error;
    	}
    	tim++;
    }

    tim = 0;

    for(int j = 0; j <5; j++)
    {
    	data[j] = 0;
    	for(int i = 8; i > 0; i--)
    	{
    		tim = 0;

    		while(!AM2301_IN_READ(am))
    		{
    			if(tim > 43)
    			{
    				return am2301_read_data_error;
    			}
    			tim++;
    		}

    		tim = 0;

    		while(AM2301_IN_READ(am))
    		{
    			if(tim > 57)
    			{
    				return am2301_read_data_error;
    			}
    			tim++;
    		}

    		if(tim < 21)
    		{
    			;
    		}
    		else
    		{
    			data[j] |= 1 << (i - 1);
    		}
    	}
    }

    if(is_parity_data_correct(data) == false)
    {
    	return am2301_parity_error;
    }

    get_temp_and_hum_from_data(am, data);

#ifdef AM2301_DEBUG
	for(int i=0; i<5; i++)
		service_print("%d ", data[i]);

	service_print("temp %d hum %d", am->temperature, am->humidity);
#endif

    return 1;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void data_pin_output(am2301_t *am)
{
    gpio_am2301.Pin = am->gpio_pin_x;
    gpio_am2301.Mode = GPIO_MODE_OUTPUT_PP;
    HAL_GPIO_Init(am->gpiox, &gpio_am2301);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void data_pin_input(am2301_t *am)
{
    gpio_am2301.Pin = am->gpio_pin_x;
    gpio_am2301.Mode = GPIO_MODE_INPUT;
    gpio_am2301.Pull = GPIO_PULLUP;
    HAL_GPIO_Init(am->gpiox, &gpio_am2301);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_parity_data_correct(uint8_t *data)
{
    if(((data[0] + data[1] + data[2] + data[3]) & 0xFF) != data[4])
    {
    	return false;
    }

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void get_temp_and_hum_from_data(am2301_t *am, uint8_t *data)
{
    am->humidity = data[0] << 8 | data[1];

    if(data[2] & 0x80)
    {
    	am->temperature = -((data[2] & 0x7F) << 8 | data[3]);
    }
    else
    {
    	am->temperature = (data[2]) << 8 | data[3];
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void delay_asm(uint32_t us)
{
	uint32_t delay_time = us;
	while(delay_time--)
	{
		asm("nop");
	}
}
