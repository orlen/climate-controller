#include "modbus_datamodel/update_modbus_dm.h"
#include "modbus_datamodel/modbus_datamodel_addr.h"
#include "modbus/modbus_port.h"

#include "modbus_commands/modbus_commands.h"
#include "log_events/log_events_controller.h"

#include "initialize_peripherials/initialize_peripherials.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define GET_PLC_OUT(id, outn) (plc_module_get_output_state(plc[id], outn))

#define GET_PLC_IN(id, inn) (plc_module_get_input_state(plc[id], inn))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                        S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void update_doc_registers();

static void update_dic_registers();

static void update_ir_registers();

static void update_hr_registers();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void update_modbus_data_model()
{
	update_doc_registers();
	update_dic_registers();
	update_ir_registers();
	update_hr_registers();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void update_doc_registers()
{														
	mc->doc_a[MDOC_CLIM_HANG_HEAT_RELAY] =
		GET_PLC_OUT(HANGAR_HEAT_RELAY_PLC_ID, HANGAR_HEAT_RELAY_PLC_OUT);

	mc->doc_a[MDOC_CLIM_HANG_AC_RELAY] =
		GET_PLC_OUT(HANGAR_AC_RELAY_PLC_ID, HANGAR_AC_RELAY_PLC_OUT);

	mc->doc_a[MDOC_CLIM_HANG_FAN_RELAY] =
		GET_PLC_OUT(HANGAR_FAN_RELAY_PLC_ID, HANGAR_FAN_RELAY_PLC_OUT);

	mc->doc_a[MDOC_CLIM_ROOF_HEAT_RELAY] =
		GET_PLC_OUT(ROOF_HEAT_RELAY_PLC_ID, ROOF_HEAT_RELAY_PLC_OUT);

	mc->doc_a[MDOC_CHARG_POWER_RELAY] =
		GET_PLC_OUT(CHARGER_POWER_RELAY_PLC_ID, CHARGER_POWER_RELAY_PLC_OUT);

	mc->doc_a[MDOC_CHARG_LOGIC_RELAY] =
		GET_PLC_OUT(CHARGER_LOGIC_RELAY_PLC_ID, CHARGER_LOGIC_RELAY_PLC_OUT);

	mc->doc_a[MDOC_IRLOCK_RELAY] =
		GET_PLC_OUT(IRLOCK_RELAY_PLC_ID, IRLOCK_RELAY_PLC_OUT);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void update_dic_registers()
{
	mc->dic_a[MDIC_PRESSURE_MIN_ALARM] =
		GET_PLC_IN(PRESSURE_MIN_ALARM_PLC_ID, PRESSURE_MIN_ALARM_PLC_IN);

	mc->dic_a[MDIC_PRESSURE_MAX_ALARM] =
		GET_PLC_IN(PRESSURE_MAX_ALARM_PLC_ID, PRESSURE_MAX_ALARM_PLC_IN);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void update_ir_registers()
{
	mc->ir_a[MIR_SYSTEM_READY] = 2;

	am2301_read_data(am2301_unl);
	mc->ir_a[MIR_CLIM_HANG_UNL_TEMP] = am2301_unl->temperature;
	mc->ir_a[MIR_CLIM_HANG_UNL_HUMD] = am2301_unl->humidity;

	am2301_read_data(am2301_upl);
	mc->ir_a[MIR_CLIM_HANG_UPL_TEMP] = am2301_upl->temperature;
	mc->ir_a[MIR_CLIM_HANG_UPL_HUMD] = am2301_upl->humidity;

	mc->ir_a[MIR_CLIM_ROOF_L_TEMP] =
			(int)(thermistor_get_temp_K(thermistor_rsl)*100);
	mc->ir_a[MIR_CLIM_ROOF_R_TEMP] =
			(int)(thermistor_get_temp_K(thermistor_rsr)*100);

	mc->ir_a[MIR_PRESSURE_STATE] = 1;

	mc->ir_a[MIR_CHARG_POWER_STATE] = mcmd_charger_power.cmd_resp;
	mc->ir_a[MIR_CHARG_LOGIC_STATE] = mcmd_charger_logic.cmd_resp;
	mc->ir_a[MIR_IRLOCK_STATE] = mcmd_irlock.cmd_resp;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void update_hr_registers()
{
	mc->hr_a[MHR_SYSLOG_ACK] =
			log_events_check_reported_log_event_ack(mc->hr_a[MHR_SYSLOG_ACK]);
}
