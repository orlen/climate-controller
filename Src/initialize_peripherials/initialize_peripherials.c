#include "initialize_peripherials/initialize_peripherials.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define PLC_OUT_SET(id,o,v) (plc_module_set_single_output(plc[id], o, v))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

plc_module_t *plc[PLC_MODULE_CNT] = { NULL } ;

TIM_HandleTypeDef plc_timer = { 0 };

uint16_t adc_values[ADC_CHANNELS] = { 0 };

am2301_t *am2301_unl = NULL;
am2301_t *am2301_upl = NULL;

thermistor_t *thermistor_rsl = NULL;
thermistor_t *thermistor_rsr = NULL;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static plc_module_t *initialize_plc_module(GPIO_TypeDef *relay_cs_port,
										   uint16_t relay_cs_pin,
										   GPIO_TypeDef *current_cs_port,
										   uint16_t current_cs_pin,
										   GPIO_TypeDef *relay_en_port,
										   uint16_t relay_en_pin,
										   open_spi_pinout_callback clbk,
                                           SPI_TypeDef *spix);

static void spi1_open(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static ADC_HandleTypeDef hadc;
static ADC_ChannelConfTypeDef adc_ch;
static DMA_HandleTypeDef hdma;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void initialize_plc(void)
{
    plc[PLC1_ID] = initialize_plc_module(PLC1_RELAY_CS_PORT, PLC1_RELAY_CS_PIN,
                                   PLC1_CURRENT_CS_PORT, PLC1_CURRENT_CS_PIN,
                                   PLC1_RELAY_EN_PORT, PLC1_RELAY_EN_PIN,
                                   spi1_open, PLC1_SPIX);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void initialize_support_peripherals_state(void)
{
	PLC_OUT_SET(HANGAR_HEAT_RELAY_PLC_ID, HANGAR_HEAT_RELAY_PLC_OUT,
				HANGAR_HEAT_RELAY_INIT_STATE);

	PLC_OUT_SET(HANGAR_AC_RELAY_PLC_ID, HANGAR_AC_RELAY_PLC_OUT,
				HANGAR_AC_RELAY_INIT_STATE);

	PLC_OUT_SET(HANGAR_FAN_RELAY_PLC_ID, HANGAR_FAN_RELAY_PLC_OUT,
				HANGAR_FAN_RELAY_INIT_STATE);

	PLC_OUT_SET(ROOF_HEAT_RELAY_PLC_ID, ROOF_HEAT_RELAY_PLC_OUT,
				ROOF_HEAT_RELAY_INIT_STATE);

	PLC_OUT_SET(CHARGER_POWER_RELAY_PLC_ID, CHARGER_POWER_RELAY_PLC_OUT,
				CHARGER_POWER_RELAY_INIT_STATE);

	PLC_OUT_SET(CHARGER_LOGIC_RELAY_PLC_ID, CHARGER_LOGIC_RELAY_PLC_OUT,
				CHARGER_LOGIC_RELAY_INIT_STATE);

	PLC_OUT_SET(IRLOCK_RELAY_PLC_ID, IRLOCK_RELAY_PLC_OUT,
				IRLOCK_RELAY_INIT_STATE);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void initialize_led_loop(void)
{
	GPIO_InitTypeDef gpio;
	gpio.Pin = LED_LOOP_PIN;
	gpio.Mode = GPIO_MODE_OUTPUT_PP;

	HAL_GPIO_Init(LED_LOOP_PORT, &gpio);
	HAL_GPIO_WritePin(LED_LOOP_PORT, LED_LOOP_PIN, 1);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void initialize_led_log_event(void)
{
	GPIO_InitTypeDef gpio;
	gpio.Pin = LOGEVT_PIN;
	gpio.Mode = GPIO_MODE_OUTPUT_PP;

	HAL_GPIO_Init(LOGEVT_PORT, &gpio);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void initialize_adc_dma(void)
{
	__HAL_RCC_ADC1_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_DMA2_CLK_ENABLE();

	GPIO_InitTypeDef gpio;
	gpio.Mode = GPIO_MODE_ANALOG;
	gpio.Pin = GPIO_PIN_2;
	gpio.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(GPIOC, &gpio);

	gpio.Mode = GPIO_MODE_ANALOG;
	gpio.Pin = GPIO_PIN_1;
	gpio.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(GPIOB, &gpio);

	hadc.Instance = ADC1;
	hadc.Init.ClockPrescaler = ADC_CLOCKPRESCALER_PCLK_DIV2;
	hadc.Init.Resolution = ADC_RESOLUTION12b;
	hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
	hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	hadc.Init.ScanConvMode = ADC_SCAN_ENABLE;
	hadc.Init.DiscontinuousConvMode = DISABLE;
	hadc.Init.ContinuousConvMode = ENABLE;
	hadc.Init.NbrOfConversion = 2;
	hadc.Init.DMAContinuousRequests = ENABLE;
	HAL_ADC_Init(&hadc);

	adc_ch.Channel = ADC_CHANNEL_12;
	adc_ch.Rank = 1;
	adc_ch.SamplingTime = ADC_SAMPLETIME_144CYCLES;
	HAL_ADC_ConfigChannel(&hadc, &adc_ch);

	adc_ch.Channel = ADC_CHANNEL_9;
	adc_ch.Rank = 2;
	adc_ch.SamplingTime = ADC_SAMPLETIME_144CYCLES;
	HAL_ADC_ConfigChannel(&hadc, &adc_ch);

	HAL_ADCEx_InjectedStart(&hadc);

	hdma.Instance = DMA2_Stream0;
	hdma.Init.Channel = DMA_CHANNEL_0;
	hdma.Init.Direction	= DMA_PERIPH_TO_MEMORY;
	hdma.Init.PeriphInc = DMA_PINC_DISABLE;
	hdma.Init.MemInc = DMA_MINC_ENABLE;
	hdma.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
	hdma.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD;
	hdma.Init.Mode = DMA_CIRCULAR;
	hdma.Init.Priority = DMA_PRIORITY_HIGH;

	HAL_DMA_Init(&hdma);
	__HAL_LINKDMA(&hadc, DMA_Handle, hdma);

	HAL_ADC_Start_DMA(&hadc, (uint32_t*)adc_values, ADC_CHANNELS);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void initialize_temp_sensors(void)
{
	am2301_unl = am2301_create();
	am2301_unl->gpio_pin_x = GPIO_PIN_2;
	am2301_unl->gpiox = GPIOF;

	am2301_upl = am2301_create();
	am2301_upl->gpio_pin_x = GPIO_PIN_1;
	am2301_upl->gpiox = GPIOF;

	thermistor_rsl = thermistor_create();
	thermistor_rsl->adc_res = 4095;
	thermistor_rsl->adc_val = &adc_values[0];
	thermistor_rsl->beta_value = 3435;
	thermistor_rsl->nom_temperature = 298.15;
	thermistor_rsl->nom_resistance = 10000;
	thermistor_rsl->ref_resistance = 10000;

	thermistor_rsr = thermistor_create();
	thermistor_rsr->adc_res = 4095;
	thermistor_rsr->adc_val = &adc_values[1];
	thermistor_rsr->beta_value = 3435;
	thermistor_rsr->nom_temperature = 298.15;
	thermistor_rsr->nom_resistance = 10000;
	thermistor_rsr->ref_resistance = 10000;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static plc_module_t *initialize_plc_module(GPIO_TypeDef *relay_cs_port,
										   uint16_t relay_cs_pin,
										   GPIO_TypeDef *current_cs_port,
										   uint16_t current_cs_pin,
										   GPIO_TypeDef *relay_en_port,
										   uint16_t relay_en_pin,
										   open_spi_pinout_callback clbk,
                                           SPI_TypeDef *spix)
{
	plc_module_t *p = NULL;

	p = plc_module_create();

	p->current_limiter_cs_port = current_cs_port;
	p->current_limiter_cs_pin = current_cs_pin;

	p->relay_cs_port = relay_cs_port;
	p->relay_cs_pin = relay_cs_pin;

	p->relay_en_out_port = relay_en_port;
	p->relay_en_out_pin = relay_en_pin;

	p->open_spi_io_clbk = clbk;

	plc_module_init(p, spix);

	return p;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void spi1_open(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};
	__HAL_RCC_SPI1_CLK_ENABLE();

	__HAL_RCC_GPIOA_CLK_ENABLE();
	/**SPI1 GPIO Configuration
	PA5     ------> SPI1_SCK
	PA6     ------> SPI1_MISO
	PA7     ------> SPI1_MOSI
	*/
	GPIO_InitStruct.Pin = GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void initialize_plc_timer()
{
	__TIM2_CLK_ENABLE();
	plc_timer.Instance = TIM2;
	plc_timer.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
	plc_timer.Init.CounterMode = TIM_COUNTERMODE_UP;
	plc_timer.Init.Prescaler = 16000-1;
	plc_timer.Init.Period = PLC_TIMER_INTERVAL_MS-1;
	plc_timer.Init.ClockDivision = 0;

	HAL_TIM_Base_Init(&plc_timer);
	__HAL_TIM_CLEAR_IT(&plc_timer, TIM_IT_UPDATE);

	HAL_NVIC_SetPriority(TIM2_IRQn, 1, 0);
	HAL_NVIC_EnableIRQ(TIM2_IRQn);
	HAL_TIM_Base_Start_IT(&plc_timer);
}
