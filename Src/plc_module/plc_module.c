#include "plc_module/plc_module.h"
#include "plc_module/x_nucleo_plc01a1/x_nucleo_plc01a1.h"
#include "debug.h"
#include <stdlib.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                        S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define ResetBitInByte(byte, bit_pos) (~(1 << bit_pos) & byte)

#define SetBitInByte(byte, bit_pos) ((1 << bit_pos) | byte)

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static HAL_StatusTypeDef open_spi(plc_module_t *plc);

static void spi_open_clock(SPI_TypeDef *spix);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

plc_module_t *plc_module_create(void)
{
	return (plc_module_t*)malloc(sizeof(plc_module_t));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void plc_module_init(plc_module_t *plc, SPI_TypeDef *spix)
{
	plc->hspi = (SPI_HandleTypeDef*)malloc(sizeof(SPI_HandleTypeDef));
	plc->hspi->Instance = spix;

	plc->set_outputs_states = 0x00;
	plc->outputs_states = 0x00;
	plc->inputs_states = 0x00;
	plc->relay_init = 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void plc_module_communication_init(plc_module_t *plc)
{
	VNI8200XP_IO_Config(plc);
	CLT01_38S_IO_Config(plc);
	open_spi(plc);

	plc_module_reset_module(plc);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void plc_module_set_outputs(plc_module_t *plc, uint8_t output_states)
{
	plc->set_outputs_states = output_states;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void plc_module_set_single_output(plc_module_t *plc, relay_out_n_t outn, 
								  uint8_t on_off)
{
	if(on_off == OFF)
	{
		plc->set_outputs_states = ResetBitInByte(plc->set_outputs_states,
												outn);
	}
	else if(on_off == ON)
	{
		plc->set_outputs_states = SetBitInByte(plc->set_outputs_states, outn);
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

RELAY_StatusTypeDef plc_module_send_outputs(plc_module_t *plc)
{
	static uint8_t* resp = NULL;
	resp = BSP_RELAY_SetOutputs(plc, &plc->set_outputs_states);

	if(!plc->relay_init)
	{
		plc->relay_init = 1;
		return RELAY_OK;
	}

	plc->relay_state = BSP_GetRelayStatus(resp);
	if(plc->relay_state == RELAY_OK)
	{
		plc->outputs_states = plc->set_outputs_states;
		plc->state = PLC_STATE_OK;
	}
	else
	{
		plc->state = PLC_STATE_RELAY_ERROR;
		plc_module_output_cycling(plc);
	}

	return plc->relay_state;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

RELAY_StatusTypeDef plc_module_set_and_send_outputs(plc_module_t *plc,
													uint8_t output_states)
{
	plc_module_set_outputs(plc, output_states);

	return plc_module_send_outputs(plc);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

RELAY_StatusTypeDef plc_module_set_and_send_single_output(plc_module_t *plc,
										   relay_out_n_t outn, uint8_t on_off)
{
	plc_module_set_single_output(plc, outn, on_off);

	return plc_module_send_outputs(plc);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

uint8_t* plc_module_read_inputs_handler(plc_module_t *plc)
{
  static uint8_t* resp = NULL;
  resp = BSP_CURRENT_LIMITER_Read(plc);

  plc->current_state = BSP_GetCurrentLimiterStatus(resp);
  if(plc->current_state != CURRENT_LIMITER_OK)
  {
	  plc->state = PLC_STATE_CURRENT_ERROR;
	  plc_module_output_cycling(plc);
  }
  else
  {
	  plc->state = PLC_STATE_OK;
  }

  plc->inputs_states = *(resp+1);

  return (resp+1);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

uint8_t plc_module_get_input_state(plc_module_t *plc, current_in_n_t inn)
{
	return ((plc->inputs_states >> inn) & 0x01);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

uint8_t plc_module_get_output_state(plc_module_t *plc, relay_out_n_t outn)
{
	return ((plc->outputs_states >> outn) & 0x01);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void plc_module_mirror_signals_handler(plc_module_t *plc)
{
	uint8_t *Input_Data = plc_module_read_inputs_handler(plc);

	plc_module_set_and_send_outputs(plc, *Input_Data);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void plc_module_print_inputs(plc_module_t *plc)
{
	if(plc->state == PLC_STATE_OK)
	{
		PLC_PRINT("inputs [1-8] : %d | %d | %d | %d | %d | %d | %d | %d",
					plc_module_get_input_state(plc, CURRENT_IN_1),
					plc_module_get_input_state(plc, CURRENT_IN_2),
					plc_module_get_input_state(plc, CURRENT_IN_3),
					plc_module_get_input_state(plc, CURRENT_IN_4),
					plc_module_get_input_state(plc, CURRENT_IN_5),
					plc_module_get_input_state(plc, CURRENT_IN_6),
					plc_module_get_input_state(plc, CURRENT_IN_7),
					plc_module_get_input_state(plc, CURRENT_IN_8));
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void plc_module_print_states(plc_module_t *plc)
{
	PLC_PRINT("plc_state %d | relay_state %d | current_state %d",
			plc->state, plc->relay_state, plc->current_state);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void plc_module_output_cycling(plc_module_t *plc)
{
	HAL_GPIO_WritePin(plc->current_limiter_cs_port,
			plc->current_limiter_cs_pin, 0);
	HAL_GPIO_WritePin(plc->current_limiter_cs_port,
			plc->current_limiter_cs_pin, 1);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void plc_module_reset_module(plc_module_t *plc)
{
	BSP_RELAY_Reset(plc);
	int wait = 1000;
	while(wait--);
	BSP_RELAY_EN_Out(plc);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static HAL_StatusTypeDef open_spi(plc_module_t *plc)
{
	HAL_StatusTypeDef ret_val;
	spi_open_clock(plc->hspi->Instance);
	plc->hspi->Init.Mode = SPI_MODE_MASTER;
	plc->hspi->Init.Direction = SPI_DIRECTION_2LINES;
	plc->hspi->Init.DataSize = SPI_DATASIZE_16BIT;
	plc->hspi->Init.CLKPolarity = SPI_POLARITY_LOW;
	plc->hspi->Init.CLKPhase = SPI_PHASE_1EDGE;
	plc->hspi->Init.NSS = SPI_NSS_SOFT;
	plc->hspi->Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
	plc->hspi->Init.FirstBit = SPI_FIRSTBIT_MSB;
	plc->hspi->Init.TIMode = SPI_TIMODE_DISABLED;
	plc->hspi->Init.CRCCalculation = SPI_CRCCALCULATION_DISABLED;

	plc->open_spi_io_clbk();
	ret_val = HAL_SPI_Init(plc->hspi);	

	if (ret_val != HAL_OK)
	{
		return ret_val;
	}

	if (HAL_SPI_GetState(plc->hspi) == HAL_SPI_STATE_READY)
	{
		return HAL_OK;
	}
	else
	{
		return HAL_ERROR;
	}

	return HAL_OK;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void spi_open_clock(SPI_TypeDef *spix)
{
	if(spix == SPI1)
	{
		__SPI1_CLK_ENABLE();
	}
	else if(spix == SPI2)
	{
		__SPI2_CLK_ENABLE();
	}
	else if(spix == SPI3)
	{
		__SPI3_CLK_ENABLE();
	}
	else if(spix == SPI4)
	{
		__SPI4_CLK_ENABLE();
	}
	else if(spix == SPI5)
	{
		__SPI5_CLK_ENABLE();
	}
	else if(spix == SPI6)
	{
		__SPI6_CLK_ENABLE();
	}
}
