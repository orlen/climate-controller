/**
  ******************************************************************************
  * @file    x_nucleo_plc01a1.c
  * @author  CL
  * @version V1.1.0
  * @date    25-April-2016
  * @brief   X-NUCLEO-PLC01A1_application.
  *
  * This file provides firmware functions which are Application Oriented
  ==============================================================================


  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2015 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "plc_module/x_nucleo_plc01a1/x_nucleo_plc01a1.h"
#include "debug.h"

/** @addtogroup BSP     BSP
  * @{
  * @brief
  */


/** @addtogroup X_NUCLEO_PLC01A1     X_NUCLEO_PLC01A1
  * @brief
  *@{
  */

/** @defgroup X_NUCLEO_PLC01A1_Exported_Functions X_NUCLEO_PLC01A1_Exported_Functions
 * @{
 */
/**
  * @brief      Mirrors input data
  * @param      Input channels data
  * @retval     Output channels data
**/
uint8_t BSP_Signal_Mirror(uint8_t input_state)
{
  return(input_state);
}


/**
  * @brief      Freeze selected output for a given time
  * @param      Input channels state
  * @param      Output channels to be freezed
  * @param      Duration of freeze in miliseconds
  * @retval     Output chanels data
**/

uint8_t BSP_Output_Freeze(uint8_t input_state, uint8_t freeze_channels, uint32_t* msec)
{
  if (*msec != 0)
  {
    return (input_state&freeze_channels);
  }
  else
  {
    return 0x00;
  }
}


/**
  * @brief      Regroup output buffer according to out_Array
  * @param      Regroup array
  * @retval     Output channels data
**/
uint8_t BSP_Output_Regroup(uint8_t Out_Array)
{
  return(Out_Array);
}


/**
  * @brief      Sum all the inputs at high state
  * @param      Input channels data
  * @retval     Value corresponding to the sum of inputs at high
**/

uint8_t BSP_Inputs_Sum(uint8_t input_state)
{
  static uint8_t inputChannelsOn = 0;
  static uint8_t count = 0;

  for(count = 0; count<8; count++)
  {
    if(input_state & 0x01)
      inputChannelsOn++;

    input_state = input_state >> 1;
  }

  return inputChannelsOn;
}


/**
  * @brief      Keep the selected channel high
  * @param      Output to keep high
  * @retval     Output value
**/
uint8_t BSP_Output_ON(uint8_t Out_Array)
{
  return(Out_Array);
}


/**
  * @brief      Keep the selected channel low
  * @param      Output to keep low
  * @retval     Output value
**/
uint8_t BSP_Output_OFF(uint8_t Out_Array)
{
  return(Out_Array);
}


/**
  * @brief      AND Inputs for selected output channels
  * @param      Input channels state
  * @param      Outputs to be AND with inputs
  * @retval     Result of AND operation
**/
uint8_t BSP_Inputs_AND(uint8_t input_state, uint8_t Out_Channel)
{
  return (input_state&Out_Channel);
}


/**
  * @brief      OR Inputs for selected output channels
  * @param      Input channels state
  * @param      Outputs to be OR with inputs
  * @retval     Result of OR operation
**/
uint8_t BSP_Inputs_OR(uint8_t input_state, uint8_t Out_Channel)
{
  return (input_state|Out_Channel);
}


/**
  * @brief        NOT Inputs
  * @param        Input channels state
  * @retval       Result of NOT operation
**/
uint8_t BSP_Inputs_NOT(uint8_t input_state)
{
  return (~input_state);
}


/**
  * @brief      XOR inputs for selected output channels
  * @param      Input channels state
  * @param      Outputs to be XOR with inputs
  * @retval     Result of XOR operation
**/
uint8_t BSP_Inputs_XOR(uint8_t input_state, uint8_t Out_Channel)
{
  return (input_state ^ Out_Channel);
}


/**
  * @brief      Reset relay
  * @param      None
  * @retval     None
**/
void BSP_RELAY_Reset(plc_module_t *plc)
{
	VNI8200XP_ResetIt(plc);
}

/**
  * @brief      Enable relay outputs
  * @param      None
  * @retval     None
**/
void BSP_RELAY_EN_Out(plc_module_t *plc)
{
	VNI8200XP_EnOut(plc);
}

void BSP_RELAY_DIS_Out(plc_module_t *plc)
{
	VNI8200XP_EnOut(plc);
}

uint8_t* BSP_RELAY_SetOutputs(plc_module_t *plc, uint8_t* output_data)
{
  return(VNI8200XP_SendOutputData(plc, *output_data));
}

uint8_t* BSP_CURRENT_LIMITER_Read(plc_module_t *plc)
{
  return(CLT01_38S_GetInpData(plc));
}


/**
  * @brief      Get status of relay
  * @param      Pointer to incoming status from relay
  * @retval     RELAY_OK if status is ok otherwise RELAY_ERROR
**/
RELAY_StatusTypeDef BSP_GetRelayStatus(uint8_t* status)
{
  if (VNI8200XP_FaultStatus(status))
  {
	  return RELAY_ERR_FAULT;
  }
  if (VNI8200XP_FbOkStatus(status))
  {
  	  return RELAY_ERR_FBOK;
  }
  if (VNI8200XP_TempWarningStatus(status))
  {
  	  return RELAY_ERR_TEMPWARN;
  }
  if (VNI8200XP_PcFailStatus(status))
  {
  	  return RELAY_ERR_PCFAIL;
  }
  if (VNI8200XP_PowerGoodStatus(status))
  {
  	  return RELAY_ERR_PWRGOOD;
  }
  if (VNI8200XP_CommErrorStatus(status))
  {
  	  return RELAY_ERR_COM;
  }

  return RELAY_OK;
}

/**
  * @brief      Get status of current limiter
  * @param      Pointer to incoming status from current limiter
  * @retval     CURRENT_LIMITER_OK if status is ok otherwise CURRENT_LIMITER_ERROR
**/
CURRENT_LIMITER_StatusTypeDef BSP_GetCurrentLimiterStatus(uint8_t* status)
{
  if (CLT01_38S_OtaStatus(status))
  {
	  return CURRENT_LIMITER_ERR_OTA;
  }
  if (CLT01_38S_UvaStatus(status))
  {
	  return CURRENT_LIMITER_ERR_UVA;
  }
  if (CLT01_38S_CommErrorStatus(status))
  {
	  return CURRENT_LIMITER_ERR_COM;
  }

  return CURRENT_LIMITER_OK;
}



/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
