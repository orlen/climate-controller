/**
  ******************************************************************************
  * @file    vni8200xp.c
  * @author  CL
  * @version V1.1.0
  * @date    25-April-2016
  * @brief   This file provides a set of functions needed to manage VNI8200XP
  * This file provides firmware functions for how  to manage I/O from VNI8200XP 
  ==============================================================================    
 
           
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************  
  */

/* Includes ------------------------------------------------------------------*/
#include "plc_module/x_nucleo_plc01a1/vni8200xp.h"
    
/** @addtogroup Drivers
  * @{
  * @brief Demo Driver Layer
  */    
  
/** @addtogroup BSP
  * @{
  */    

/** @defgroup VNI8200XP_Private_Functions       VNI8200XP_Private_Functions
  * @{
  */
static void gpio_open_clock(GPIO_TypeDef *gpiox);



/**
 * @brief  Configures VNI8200XP gpios for NUCLEO boards
 * @param  None
 * @retval None
 */
void VNI8200XP_IO_Config(plc_module_t *plc)
{
  GPIO_InitTypeDef GPIO_InitStruct;

  gpio_open_clock(plc->relay_cs_port);
  gpio_open_clock(plc->relay_en_out_port);

  GPIO_InitStruct.Pin = plc->relay_cs_pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(plc->relay_cs_port, &GPIO_InitStruct);
  HAL_GPIO_WritePin(plc->relay_cs_port, plc->relay_cs_pin, 1);

  GPIO_InitStruct.Pin = plc->relay_en_out_pin;
  HAL_GPIO_Init(plc->relay_en_out_port, &GPIO_InitStruct);
  HAL_GPIO_WritePin(plc->relay_en_out_port, plc->relay_en_out_pin, 0);
}

/**
 * @brief  Set reset GPIO high to disable VNI8200XP
 * @param  None
 * @retval None
 */
void VNI8200XP_ResetIt(plc_module_t *plc)
{
	HAL_GPIO_WritePin(plc->current_limiter_cs_port,
				      plc->current_limiter_cs_pin,
					  GPIO_PIN_RESET);
	HAL_GPIO_WritePin(plc->current_limiter_cs_port,
					  plc->current_limiter_cs_pin,
					  GPIO_PIN_SET);
}


/**
 * @brief  Enable outputs of VNI8200XP
 * @param  None
 * @retval None
 */
void VNI8200XP_EnOut(plc_module_t *plc)
{
	HAL_GPIO_WritePin(plc->relay_en_out_port,
					  plc->relay_en_out_pin,
					  GPIO_PIN_SET);
}

void VNI8200XP_DisOut(plc_module_t *plc)
{
	HAL_GPIO_WritePin(plc->relay_en_out_port,
					  plc->relay_en_out_pin,
					  GPIO_PIN_RESET);
}


/**
 * @brief  Calculate output parity bits and send data
 * @param  None
 * @retval None
 */
uint8_t* VNI8200XP_SendOutputData(plc_module_t *plc, uint8_t udata)
{
  static uint8_t P0, P1, P2, nP0, parityData, txData[2], rxData[2];

  P0 = udata^(udata>>1);
  P0 = P0 ^(P0 >> 2);
  P0 = P0 ^(P0 >> 4);
  P0 = P0 & 0x01;

  P1 = udata^(udata>>2);
  P1 = P1 ^ (P1 >> 4);

  P2 = P1 & 0x01;

  P1 = P1 & 0x02;
  P1 = P1 >> 1;

  nP0 = (~P0) & 0x01;

  parityData = (P2<<3)|(P1<<2)|(P0<<1)|nP0;

  *txData = parityData;
  *(txData+1) = udata;

  HAL_GPIO_WritePin(plc->relay_cs_port, plc->relay_cs_pin, GPIO_PIN_RESET);
  HAL_SPI_TransmitReceive(plc->hspi,txData,rxData,1,0x100);
  HAL_GPIO_WritePin(plc->relay_cs_port, plc->relay_cs_pin, GPIO_PIN_SET);

  return rxData;
}


/**
 * @brief  Calculate output parity bits and send data
 * @param  None
 * @retval None
 */
uint8_t VNI8200XP_FaultStatus(uint8_t* status)
{
  return (*(status+1) != 0);
}


/**
 * @brief  Returns status of feedback
 * @param  VNI8200XP incoming status
 * @retval Status of feedback
 */
uint8_t VNI8200XP_FbOkStatus(uint8_t* status)
{
  return (((*status)&0x80) == 0);
}

/**
 * @brief  Returns status of Temperature warning
 * @param  VNI8200XP incoming status
 * @retval Status of Temperature Warning
 */
uint8_t VNI8200XP_TempWarningStatus(uint8_t* status)
{
  return (((*status)&0x40) != 0);
}


/**
 * @brief  Returns status of Outgoing Parity check
 * @param  VNI8200XP incoming status
 * @retval Status of parity check
 */
uint8_t VNI8200XP_PcFailStatus(uint8_t* status)
{
  return (((*status)&0x20) != 0);
}


/**
 * @brief  Returns status of power to VNI8200XP
 * @param  VNI8200XP incoming status
 * @retval Status of power good
 */
uint8_t VNI8200XP_PowerGoodStatus(uint8_t* status)
{
  return (((*status)&0x10) != 0);
}


/**
 * @brief  Returns status of incoming parity
 * @param  VNI8200XP incoming status
 * @retval Result of incoming parity check
 */
uint8_t VNI8200XP_CommErrorStatus(uint8_t* status)
{
  static uint16_t P0, P1, P2, nP0, tempStatusData, parityCalcData;

  tempStatusData = ((*status))+((*(status+1))*256);
  tempStatusData = tempStatusData & 0xFFF0;

  P0 = tempStatusData^(tempStatusData>>1);
  P0 = P0 ^(P0 >> 2);
  P0 = P0 ^(P0 >> 4);
  P0 = P0 & 0x0100;
  P0 = P0 >> 8;

  P1 = (tempStatusData)^(tempStatusData>>2);
  P1 = P1 ^ (P1 >> 4);
  P1 = P1 ^ (P1 >> 8);

  P2 = P1 & 0x01;

  P1 = P1 & 0x02;
  P1 = P1 >> 1;

  nP0 = (~P0) & 0x01;

  parityCalcData = (P2<<3)|(P1<<2)|(P0<<1)|nP0;

  return (((uint8_t)parityCalcData) != ((*status)&0x0F));
}

static void gpio_open_clock(GPIO_TypeDef *gpiox)
{
	if(gpiox == GPIOA)
	{
		__GPIOA_CLK_ENABLE();
	}
	else if(gpiox == GPIOB)
	{
		__GPIOB_CLK_ENABLE();
	}
	if(gpiox == GPIOC)
	{
		__GPIOC_CLK_ENABLE();
	}
	else if(gpiox == GPIOD)
	{
		__GPIOD_CLK_ENABLE();
	}
	if(gpiox == GPIOE)
	{
		__GPIOE_CLK_ENABLE();
	}
	else if(gpiox == GPIOF)
	{
		__GPIOF_CLK_ENABLE();
	}
	if(gpiox == GPIOG)
	{
		__GPIOG_CLK_ENABLE();
	}
	else if(gpiox == GPIOH)
	{
		__GPIOH_CLK_ENABLE();
	}
}

/**
  * @}
  */
/**
  * @}
   */
/**
  * @}
  */

/**
  * @}
  */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
