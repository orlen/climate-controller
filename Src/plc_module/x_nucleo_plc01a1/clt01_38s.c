/**
  ******************************************************************************
  * @file    clt01_38s.c
  * @author  CL
  * @version V1.1.0
  * @date    25-April-2016
  * @brief   This file provides a set of function to manage CLT01-38SQ7
  ==============================================================================    
 
           
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2015 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************  
  */

/* Includes ------------------------------------------------------------------*/
#include "plc_module/x_nucleo_plc01a1/clt01_38s.h"
    
/** @addtogroup Drivers     Drivers
  * @{
  * @brief Demo Driver Layer
  */    
  
/** @addtogroup BSP     BSP
  * @{
  */    
    
/** @addtogroup Components     Components
  * @{
  */

/** @defgroup CLT01_38S    CLT01_38S
  * @{ 
  */


/** @defgroup CLT01_38S_Private_variables     CLT01 38S Private variables
  * @{
  * @brief Digital Input Private variables
  */    

/**
  * @}
  */

static void gpio_open_clock(GPIO_TypeDef *gpiox);


/** @defgroup CLT01_38S_Private_Functions     CLT01 38S Private Functions
  *  @{
    * @brief Digital Input exported Function 
  */  

/**
 * @brief  Set CLT01 Initialization
 * @param  CLT01_Init configuration setting for the CLT01
 * @retval CURRENT_LIMITER_OK in case of success, an error code otherwise
 */

/**
 * @brief  Configures CLT01-38SQ7 gpios
 * @param  None
 * @retval None
 */
void CLT01_38S_IO_Config(plc_module_t *plc)
{
  GPIO_InitTypeDef GPIO_InitStruct;

  gpio_open_clock(plc->current_limiter_cs_port);

  GPIO_InitStruct.Pin = plc->current_limiter_cs_pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW ;
  HAL_GPIO_Init(plc->current_limiter_cs_port, &GPIO_InitStruct);
}


/**
 * @brief  Read input channels state
 * @param  None
 * @retval Pointer to input data from CLT01-38SQ7
 */
uint8_t* CLT01_38S_GetInpData(plc_module_t *plc)
{
  static uint8_t txData[2],rxData[2];

  HAL_GPIO_WritePin(plc->current_limiter_cs_port, plc->current_limiter_cs_pin, GPIO_PIN_RESET);
  HAL_SPI_TransmitReceive(plc->hspi, txData, rxData, 1, 0x100);
  HAL_GPIO_WritePin(plc->current_limiter_cs_port, plc->current_limiter_cs_pin, GPIO_PIN_SET);

  return rxData;
}


/**
 * @brief  Returns status of incoming parity
 * @param  Pointer to CLT01-38SQ7 incoming status
 * @retval Result of incoming parity check status
 */
uint8_t CLT01_38S_CommErrorStatus(uint8_t* status)
{
  static uint8_t PC1, PC2, PC3, PC4, parityCalcData;

  PC1 = (*(status+1))^((*(status+1))>>1);
  PC1 = PC1 ^ (PC1 >> 2);

  PC2 = PC1 & 0x10;
  PC2 = PC2 >> 4;

  PC3 = PC1 & 0x01;

  PC4 = PC1 & 0x04;
  PC4 = PC4 >> 2;

  PC1 = PC1 ^ (PC1 >> 4);
  PC1 = PC1 & 0x01;

  PC1 = ~PC1; PC1 = PC1 & 0x01;
  PC2 = ~PC2; PC2 = PC2 & 0x01;
  PC3 = ~PC3; PC3 = PC3 & 0x01;
  PC4 = ~PC4; PC4 = PC4 & 0x01;

  parityCalcData = (PC1<<5)|(PC2<<4)|(PC3<<3)|(PC4<<2);

  return (((uint8_t)parityCalcData) != ((*status)&0x3C));
}


/**
 * @brief  Get status of under voltage alarm
 * @param  Pointer to CLT01-38SQ7 incoming status
 * @retval Under voltage alarm status
 */
uint8_t CLT01_38S_UvaStatus(uint8_t* status)
{
  return (((*status)&0x80) == 0);
}


/**
 * @brief  Get status of over temperature alarm
 * @param  Pointer to CLT01-38SQ7 incoming status
 * @retval Over Temperature alarm status
 */
uint8_t CLT01_38S_OtaStatus(uint8_t* status)
{
  return (((*status)&0x40) == 0);
}


static void gpio_open_clock(GPIO_TypeDef *gpiox)
{
	if(gpiox == GPIOA)
	{
		__GPIOA_CLK_ENABLE();
	}
	else if(gpiox == GPIOB)
	{
		__GPIOB_CLK_ENABLE();
	}
	if(gpiox == GPIOC)
	{
		__GPIOC_CLK_ENABLE();
	}
	else if(gpiox == GPIOD)
	{
		__GPIOD_CLK_ENABLE();
	}
	if(gpiox == GPIOE)
	{
		__GPIOE_CLK_ENABLE();
	}
	else if(gpiox == GPIOF)
	{
		__GPIOF_CLK_ENABLE();
	}
	if(gpiox == GPIOG)
	{
		__GPIOG_CLK_ENABLE();
	}
	else if(gpiox == GPIOH)
	{
		__GPIOH_CLK_ENABLE();
	}
}
/**
  * @}
  */

/**
  * @}
  */
/**
  * @}
   */
/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
