#include "support_control.h"
#include "debug.h"
#include "initialize_peripherials/initialize_peripherials.h"
#include "log_events/log_events_controller.h"
#include "modbus/modbus_port.h"
#include "modbus_datamodel/update_modbus_dm.h"
#include "modbus_commands/modbus_commands.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static volatile bool plc_rw_flag = true;

static bool init_modbus = false;

static bool init_plc = false;

static int led_loop_toggle_cnt = 1000;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void plc_data_handler(void);

static void led_loop_toggle(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void support_initialize(void)
{
	//initialize_adc_dma();
	modbus_communication_initialize();
	initialize_plc();

	initialize_support_peripherals_state();
	initialize_led_log_event();
	initialize_led_loop();

	plc_module_communication_init(plc[PLC1_ID]);
	initialize_plc_timer();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void support_control(void)
{
	plc_data_handler();

	if(!init_modbus)
	{
		modbus_transmit_enable();
		init_modbus = true;
	}

	modbus_data_handler(mc);
	update_modbus_data_model();
	modbus_commands_handler();
//	log_events_controller_handler();

	led_loop_toggle();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void plc_data_handler(void)
{
	if(plc_rw_flag)
	{
		plc_module_read_inputs_handler(plc[PLC1_ID]);
		plc_module_send_outputs(plc[PLC1_ID]);
		plc_rw_flag = false;
	}

	plc_module_output_cycling(plc[PLC1_ID]);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void led_loop_toggle(void)
{
	if(led_loop_toggle_cnt == 0)
	{
		HAL_GPIO_TogglePin(LED_LOOP_PORT, LED_LOOP_PIN);
		led_loop_toggle_cnt = 1000;
	}
	else
	{
		led_loop_toggle_cnt--;
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//              P E R I P H E R I A L S   I R Q   H A N D L E R S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	modbus_usart_rx_cplt(huart);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void plc_timer_period_elapsed(TIM_HandleTypeDef *htim)
{
	if(htim->Instance == TIM2)
	{
		plc_rw_flag = true;
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	modbus_tim_period_elapsed(htim);
	plc_timer_period_elapsed(htim);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void TIM2_IRQHandler()
{
	HAL_TIM_IRQHandler(&plc_timer);
}
