#include "log_events/log_events_controller.h"
#include "log_events/log_list/log_list.h"

#include "debug.h"

#include <stdio.h>
#include <stdbool.h>


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define REPORTED_LE_ID 0

#define NONE_ACK 0x0000
#define LE_ACK   0x0001

enum {REPORT_ON, REPORT_OFF};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static log_event_t *le_list[] = 
{

};

static uint8_t le_cnt = 0;

static log_event_t *triggered_le[255] = { NULL };

static uint8_t triggered_le_cnt = 0;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void check_log_events_condition();

static void report_log_events();

static void add_triggered_log_event(log_event_t *le);

static void delete_reported_log_event(void);

static bool is_new_log_event_triggered(log_event_t *le);

static bool is_any_log_event_reported(log_event_t *le);

static void hardware_report_signal(uint8_t report_state);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void log_events_controller_handler()
{
    check_log_events_condition();
    report_log_events();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

uint16_t log_events_get_reported_log_code()
{
    if(is_any_log_event_reported(triggered_le[REPORTED_LE_ID]) == true)
    {
        return triggered_le[REPORTED_LE_ID]->code;
    }
    
    return 0x0000;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

uint16_t log_events_check_reported_log_event_ack(uint16_t ack_val)
{
    if(ack_val == LE_ACK)
    {
        triggered_le[REPORTED_LE_ID]->state = LOG_EVENT_STATE_RECEIVED;

        hardware_report_signal(REPORT_OFF);
    }
    return NONE_ACK;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void check_log_events_condition()
{
    for(int le_id = 0; le_id < le_cnt; le_id++)
    {
        if(is_new_log_event_triggered(le_list[le_id]) == true)
        {
        	add_triggered_log_event(le_list[le_id]);
        }
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void report_log_events()
{
    if(triggered_le_cnt > 0)
    {
        log_event_state_t state = triggered_le[REPORTED_LE_ID]->state;
        if(state == LOG_EVENT_STATE_TRIGGERED)
        {
            triggered_le[REPORTED_LE_ID]->state = LOG_EVENT_STATE_REPORTED;           

            hardware_report_signal(REPORT_ON);
        }
        else if(state == LOG_EVENT_STATE_RECEIVED)
        {
        	if(triggered_le[REPORTED_LE_ID]->ackhandler_clbk != NULL)
        	{
        		triggered_le[REPORTED_LE_ID]->ackhandler_clbk(
        				triggered_le[REPORTED_LE_ID]);
        	}

            triggered_le[REPORTED_LE_ID]->state = LOG_EVENT_STATE_IDLE;
            delete_reported_log_event();
        }        
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void add_triggered_log_event(log_event_t *le)
{
    service_print("Add triggered log_event with code %d", le->code);
    triggered_le[triggered_le_cnt++] = le;
    le->state = LOG_EVENT_STATE_TRIGGERED;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void delete_reported_log_event(void)
{
    if(triggered_le_cnt > 0)
    {
        service_print("Delete reported log_event %d", 
            triggered_le[REPORTED_LE_ID]->code);

        triggered_le_cnt -= 1;

        for(int rle_id = 0; rle_id < triggered_le_cnt; rle_id++)
        {
            triggered_le[rle_id] = triggered_le[rle_id+1];
        }
        triggered_le[triggered_le_cnt] = NULL;

        service_print("%d left triggered log_events", triggered_le_cnt);
    }    
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_new_log_event_triggered(log_event_t *le)
{
    if(le == NULL || le->condition_clbk == NULL)
    {
        return false;
    }

    return le->condition_clbk(le) == true 
            && le->state == LOG_EVENT_STATE_IDLE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_any_log_event_reported(log_event_t *le)
{
    return triggered_le_cnt > 0 
            && le->state == LOG_EVENT_STATE_REPORTED;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void hardware_report_signal(uint8_t report_state)
{
    if(report_state == REPORT_OFF)
    {
        //HAL_GPIO_WritePin(LOGEVT_PORT, LOGEVT_PIN, 0);
        int cnt_tim = 100000;
        while(cnt_tim--);
    }
    else if(report_state == REPORT_ON)
    {
        //HAL_GPIO_WritePin(LOGEVT_PORT, LOGEVT_PIN, 1);
    }
}
