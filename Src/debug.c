/**
  ******************************************************************************
  * @file           : debug.c
  * @brief          : Debug print functions via serial (USART)
  * @author			: Mateusz Strojek
  ******************************************************************************
  *
  * COPYRIGHT(c) 2019 Cervi Robotics
  */

#include "debug.h"

#define SERVICE_DEBUG

UART_HandleTypeDef huart3;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void vprint(const char *fmt, va_list argp)
{
    char string[255];
    if(0 < vsprintf(string,fmt,argp))
    {
        HAL_UART_Transmit(&huart3, (uint8_t*)string, strlen(string), 0xffffff);
    }
    char *nl = "\r\n";
    HAL_UART_Transmit(&huart3, (uint8_t*)nl, strlen(nl), 0xffffff);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void service_print(const char *fmt, ...)
{
	#ifdef SERVICE_DEBUG
		va_list argp;
		va_start(argp, fmt);
		vprint(fmt, argp);
		va_end(argp);
	#endif
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
void MX_USART3_UART_Init(void)
{
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 115200;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    //Error_Handler();
  }
}
