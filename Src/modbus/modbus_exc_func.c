#include<stdio.h>

#include"modbus/modbus_exc_func.h"
#include"modbus/modbus_util.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusExceptionCode_t modbus_valid_request(ModbusCom *mc)
{
    ModbusExceptionCode_t exc_code = MEC_None;
    uint8_t func_code = ModbusFrameFuncCode(mc);

    #ifdef MODBUS_DEBUG
        MODBUS_DEBUG_CLBK("----------------------\n");
        MODBUS_DEBUG_CLBK("modbus_valid_request()\n");
        MODBUS_DEBUG_CLBK("func_code %d\n", func_code);
    #endif   

    if(modbus_exc_is_illegal_function(mc, func_code) == MES_Illegal)
    {
        exc_code = MEC_IllegalFunction;
    }
    else if(modbus_exc_is_illegal_data_value(mc, func_code) == MES_Illegal)
    {
        exc_code = MEC_IllegalDataValue;
    }
    else if(modbus_exc_is_illegal_data_address(mc, func_code) == MES_Illegal)
    {
        exc_code = MEC_IllegalDataAddress;
    }

    #ifdef MODBUS_DEBUG
        MODBUS_DEBUG_CLBK("exc_code %d\n", exc_code);
    #endif

    return exc_code;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusExceptionState_t modbus_exc_is_crc_correct(uint8_t *adu, uint16_t adu_size)
{
    uint16_t rec_crc = ModbusReceiveCrc(adu, adu_size);
    //calculate crc for ADU frame without receive CRC 2-bytes
    uint16_t calc_crc = modbus_crc_calculate(adu, adu_size-2);

    #ifdef MODBUS_DEBUG
        MODBUS_DEBUG_CLBK("----------------------------\n");
        MODBUS_DEBUG_CLBK("modbus_exc_is_crc_correct()\n");
        MODBUS_DEBUG_CLBK("rec_crc = %#06X | calc_crc = %#06X | r = %d\n", 
            rec_crc, calc_crc, rec_crc == calc_crc);
    #endif

    if(rec_crc == calc_crc)
    {
        return MES_Correct;
    }
    else
    {
        return MES_Illegal;
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
/**
 * @brief   Check is the function code from receive request frame is supported 
 *          in this configure Modbus Communication. Exception code = 0x01.
 * @param   func_code: function code from request PDU frame
 * @retval  Modbus Exception State
 */
ModbusExceptionState_t modbus_exc_is_illegal_function(ModbusCom *mc, 
    uint8_t func_code)
{
    for(int func_nr = 0; func_nr < mc->supported_func_cnt; func_nr++)
    {
        if(mc->supported_func_a[func_nr] == func_code)
        {
            return MES_Correct;
        }
    }
    return MES_Illegal;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
/**
 * @brief   Check Illegal Data Value Exception in receive request frame for all 
 *          supported functions with required condition. Exception code = 0x03.
 * @param   func_code: function code from request PDU frame
 * @param   val: data value from request PDU frame   
 * @retval  Modbus Exception State
 */
ModbusExceptionState_t modbus_exc_is_illegal_data_value(ModbusCom *mc, 
    uint8_t func_code)
{
    uint16_t qnt;
    uint16_t byte_cnt; 

    qnt = ModbusFrameQntByte(mc);

    if(func_code == MFC_WriteMultipleCoils 
        || func_code == MFC_WriteMultipleRegisters)
    {
        byte_cnt = ModbusFrameByteCntByte(mc);
    }

    return (func_code == MFC_ReadCoils 
                && (qnt >= MODBUS_R_COILS_QNT_MIN 
                && qnt <= MODBUS_R_COILS_QNT_MAX))

            || (func_code == MFC_ReadDiscreteInputs 
                && (qnt >= MODBUS_CONTACTS_QNT_MIN 
                && qnt <= MODBUS_CONTACTS_QNT_MAX))

            || (func_code == MFC_ReadHoldingRegisters 
                && (qnt >= MODBUS_R_HREG_QNT_MIN
                && qnt <= MODBUS_R_HREG_QNT_MAX))

            || (func_code == MFC_ReadInputRegister
                && (qnt >= MODBUS_IREG_QNT_MIN 
                && qnt <= MODBUS_IREG_QNT_MAX))
  
            || (func_code == MFC_WriteSingleCoil
                && (qnt == MODBUS_COIL_VALUE_HIGH 
                || qnt == MODBUS_COIL_VALUE_LOW))

            || (func_code == MFC_WriteSingleRegister
                && (qnt >= MODBUS_HREG_MIN_VAL 
                && qnt <= MODBUS_HREG_MAX_VAL))

            || (func_code == MFC_WriteMultipleCoils
                && (qnt >= MODBUS_W_COILS_QNT_MIN 
                && qnt <= MODBUS_W_COILS_QNT_MAX)
                && (byte_cnt == ModbusCoilsContactsByteCnt(qnt)))

            || (func_code == MFC_WriteMultipleRegisters
                && (qnt >= MODBUS_W_HREG_QNT_MIN 
                && qnt <= MODBUS_W_HREG_QNT_MAX)
                && (byte_cnt == qnt * 2));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
/**
 * @brief   Check Illegal Data Address Exception in receive request frame for all
 *          supported functions with required condition. Exception code = 0x02.
 * @param   func_code: function code from request PDU frame
 * @param   addr_1: data addr from request PDU frame 
 * @param   addr_2:   
 * @retval  Modbus Exception State
 */
ModbusExceptionState_t modbus_exc_is_illegal_data_address(ModbusCom *mc, 
    uint8_t func_code)
{
    uint16_t start_addr;
    uint16_t qnt; 

    start_addr = ModbusFrameAddrByte(mc);
    qnt = ModbusFrameQntByte(mc);

    return (func_code == MFC_ReadCoils 
                && (start_addr >= MODBUS_COILS_ADDR_MIN 
                && start_addr <= mc->doc_size)
                && start_addr+qnt <= mc->doc_size)
            
            || (func_code == MFC_ReadDiscreteInputs 
                && (start_addr >= MODBUS_CONTACTS_ADDR_MIN
                && start_addr <= mc->dic_size)
                && start_addr+qnt <= mc->dic_size)
            
            || (func_code == MFC_ReadHoldingRegisters 
                && (start_addr >= MODBUS_HREG_ADDR_MIN
                && start_addr <= mc->hr_size)
                && start_addr+qnt <= mc->hr_size)

            || (func_code == MFC_ReadInputRegister
                && (start_addr >= MODBUS_IREG_ADDR_MIN
                && start_addr <= mc->ir_size)
                && start_addr+qnt <= mc->ir_size)
                
            || (func_code == MFC_WriteSingleCoil
                && (start_addr >= MODBUS_COILS_ADDR_MIN
                && start_addr < mc->doc_size))
                
            || (func_code == MFC_WriteSingleRegister
                && (start_addr >= MODBUS_HREG_ADDR_MIN
                && start_addr < mc->hr_size))
            
            || (func_code == MFC_WriteMultipleCoils
                && (start_addr >= MODBUS_COILS_ADDR_MIN
                && start_addr <= mc->doc_size)
                && (start_addr+qnt <= mc->doc_size))

            || (func_code == MFC_WriteMultipleRegisters
                && (start_addr >= MODBUS_HREG_ADDR_MIN
                && start_addr <= mc->hr_size)
                && (start_addr+qnt <= mc->hr_size)); 
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusExceptionState_t modbus_exc_is_server_device_failure(uint8_t func_code)
{
    /* TODO */
    /*uint16_t addr;
    return (func_code == MFC_ReadCoils 
                && (addr >= MODBUS_COILS_ADDR_MIN 
                && addr <= MODBUS_COILS_ADDR_MAX))
            
            || (func_code == MFC_ReadDiscreteInputs 
                && (addr >= MODBUS_CONTACTS_ADDR_MIN
                && addr <= MODBUS_CONTACTS_ADDR_MAX))
            
            || (func_code == MFC_ReadHoldingRegisters 
                && (addr >= MODBUS_HREG_ADDR_MIN
                && addr <= MODBUS_HREG_ADDR_MAX))

            || (func_code == MFC_ReadInputRegister
                && (addr >= MODBUS_IREG_ADDR_MIN
                && addr <= MODBUS_IREG_ADDR_MAX)); */
	return MES_Illegal;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusExceptionState_t modbus_exc_is_timeout_respond(ModbusCom *mc)
{
    return MES_Correct;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusExceptionState_t modbus_exc_is_rsp_is_req_echo(ModbusCom *mc)
{
    if(mc->rec_buff_size != mc->adu_size)
    {
        return MES_Illegal;
    }
    else
    {
        for(int byte_nr = 0; byte_nr < mc->rec_buff_size; byte_nr++)
        {
            if(mc->rec_buff[byte_nr] != mc->adu[byte_nr])
            {
                return MES_Illegal;
            }
        }
        return MES_Correct;
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusExceptionState_t modbus_exc_is_correct_rsp_for_mult_write(ModbusCom *mc)
{   
    if(mc->rec_buff_size == MODBUS_MULTIPLE_WRITE_RSP_SIZE)
    {
        for(int byte_nr = 0; byte_nr < MODBUS_MULTIPLE_WRITE_RSP_SIZE; 
            byte_nr++)
        {
            if(mc->rec_buff[byte_nr] != mc->adu[byte_nr])
            {
                return MES_Illegal;
            }
        }
        return MES_Correct;
    }
    else 
    {
        return MES_Illegal;
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusExceptionState_t modbus_exc_check_rsp_exception(ModbusCom *mc)
{
    uint8_t recv_fc = mc->rec_buff[MFI_FuncCode];
    uint8_t req_fc = mc->adu[MFI_FuncCode];
    uint8_t exc_func = recv_fc > MODBUS_ERROR_CODE ? 
                                            recv_fc - MODBUS_ERROR_CODE 
                                            : recv_fc;

    if(exc_func < recv_fc)
    {
        #ifdef MODBUS_DEBUG
            MODBUS_DEBUG_CLBK("Exception in rsp [func %d code %d]\n",
                exc_func, mc->rec_buff[MFI_ExceptionCode]);
        #endif

        return MES_Illegal;
    }

    if(exc_func != req_fc)
    {
        #ifdef MODBUS_DEBUG
            MODBUS_DEBUG_CLBK("Incorrect func code in rsp [req %d rsp %d]\n",
                req_fc, exc_func);
        #endif
    
        return MES_Illegal;
    }
    return MES_Correct;
}
