#include<stdio.h>
#include<stdlib.h>

#include"modbus/modbus_req_func.h"
#include"modbus/modbus_util.h"
#include"modbus/modbus_data_model_func.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void modbus_build_request(ModbusCom *mc, uint8_t transmit_addr, 
    uint8_t *pdu_data, uint8_t pdu_size);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_req_read_coils(ModbusCom *mc, uint8_t transmit_addr, 
    uint16_t start_addr, uint16_t qnt_coils)
{
    uint8_t pdu_data[] = 
    {   
        MFC_ReadCoils,

        HiByteU16(start_addr),
        LoByteU16(start_addr),

        HiByteU16(qnt_coils),
        LoByteU16(qnt_coils)
    };
    
    uint8_t pdu_size = ArraySize(pdu_data);

    modbus_build_request(mc, transmit_addr, pdu_data, pdu_size);   
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_req_read_discrete_inputs(ModbusCom *mc, uint8_t transmit_addr, 
    uint16_t start_addr, uint16_t qnt_inputs)
{
    uint8_t pdu_data[] = 
    {   
        MFC_ReadDiscreteInputs,

        HiByteU16(start_addr),
        LoByteU16(start_addr),

        HiByteU16(qnt_inputs),
        LoByteU16(qnt_inputs)
    };
    
    uint8_t pdu_size = ArraySize(pdu_data);

    modbus_build_request(mc, transmit_addr, pdu_data, pdu_size);    
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_req_read_holding_registers(ModbusCom *mc, uint8_t transmit_addr, 
    uint16_t start_addr, uint16_t qnt_registers)
{
    uint8_t pdu_data[] = 
    {   
        MFC_ReadHoldingRegisters,

        HiByteU16(start_addr),
        LoByteU16(start_addr),

        HiByteU16(qnt_registers),
        LoByteU16(qnt_registers)
    };
    
    uint8_t pdu_size = ArraySize(pdu_data);

    modbus_build_request(mc, transmit_addr, pdu_data, pdu_size);    
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_req_read_input_registers(ModbusCom *mc, uint8_t transmit_addr, 
    uint16_t start_addr, uint16_t qnt_in_registers)
{
    uint8_t pdu_data[] = 
    {   
        MFC_ReadInputRegister,

        HiByteU16(start_addr),
        LoByteU16(start_addr),

        HiByteU16(qnt_in_registers),
        LoByteU16(qnt_in_registers)
    };
    
    uint8_t pdu_size = ArraySize(pdu_data);

    modbus_build_request(mc, transmit_addr, pdu_data, pdu_size);    
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_req_write_single_coil(ModbusCom *mc, uint8_t transmit_addr, 
    uint16_t out_addr, uint16_t out_val)
{
    uint8_t pdu_data[] = 
    {   
        MFC_WriteSingleCoil,
        
        HiByteU16(out_addr),
        LoByteU16(out_addr),

        HiByteU16(out_val),
        LoByteU16(out_val)
    };
    
    uint8_t pdu_size = ArraySize(pdu_data);

    modbus_build_request(mc, transmit_addr, pdu_data, pdu_size);    
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_req_write_single_register(ModbusCom *mc, uint8_t transmit_addr, 
    uint16_t reg_addr, uint16_t reg_val)
{
    uint8_t pdu_data[] = 
    {   
        MFC_WriteSingleRegister,

        HiByteU16(reg_addr),
        LoByteU16(reg_addr),

        HiByteU16(reg_val),
        LoByteU16(reg_val)
    };
    
    uint8_t pdu_size = ArraySize(pdu_data);

    modbus_build_request(mc, transmit_addr, pdu_data, pdu_size);    
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_req_read_exception_status(ModbusCom *mc, uint8_t transmit_addr)
{   
    uint8_t pdu_data[] = 
    {   
        MFC_ReadExceptionStatus,
    };
    
    uint8_t pdu_size = ArraySize(pdu_data);

    modbus_build_request(mc, transmit_addr, pdu_data, pdu_size);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_req_diagnostics(ModbusCom *mc, uint8_t transmit_addr, 
    ModbusDiagnosticsSubfunction_t mds, uint16_t data)
{
    uint8_t pdu_data[] = 
    {      
        MFC_Diagnostics,

        HiByteU16(mds),
        LoByteU16(mds),

        HiByteU16(data),
        LoByteU16(data)
    };
    
    uint8_t pdu_size = ArraySize(pdu_data);

    modbus_build_request(mc, transmit_addr, pdu_data, pdu_size);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_req_get_comm_event_counter(ModbusCom *mc, uint8_t transmit_addr)
{   
    uint8_t pdu_data[] = 
    {   
        MFC_GetComEventCnt
    };
    
    uint8_t pdu_size = ArraySize(pdu_data);

    modbus_build_request(mc, transmit_addr, pdu_data, pdu_size);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_req_get_comm_event_log(ModbusCom *mc, uint8_t transmit_addr)
{   
    uint8_t pdu_data[] = 
    {   
        MFC_GetComEventLog
    };
    
    uint8_t pdu_size = ArraySize(pdu_data);

    modbus_build_request(mc, transmit_addr, pdu_data, pdu_size);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_req_write_multiple_coils(ModbusCom *mc, uint8_t transmit_addr, 
    uint16_t start_addr, uint16_t qnt_out, uint8_t *out_val)
{
	uint8_t byte_cnt = qnt_out % 8 == 0 ? qnt_out / 8 : (qnt_out / 8) + 1;

    uint8_t param_size = sizeof(uint8_t) + (transmit_addr)
        + sizeof(start_addr) + sizeof(qnt_out) 
        + sizeof(byte_cnt) + (sizeof(byte_cnt) * byte_cnt);

    uint8_t *pdu_data = malloc(sizeof(uint8_t) * param_size);
    uint16_t pdu_id = 0;

    pdu_data[pdu_id++] = MFC_WriteMultipleCoils;

    pdu_data[pdu_id++] = HiByteU16(start_addr);
    pdu_data[pdu_id++] = LoByteU16(start_addr);

    pdu_data[pdu_id++] = HiByteU16(qnt_out);
    pdu_data[pdu_id++] = LoByteU16(qnt_out);

    pdu_data[pdu_id++] = byte_cnt;
    printf("byte_cnt %d %d\n", byte_cnt, pdu_data[pdu_id-1]);

    uint8_t *val_bytes = modbus_get_coils_contacts_status(0, qnt_out, byte_cnt, out_val);

    for(int i = 0; i < byte_cnt; i++)
    {
        pdu_data[pdu_id++] = val_bytes[i];
    }
    
    uint8_t pdu_size = pdu_id-1;
    modbus_build_request(mc, transmit_addr, pdu_data, pdu_size);

    free(pdu_data);
    free(val_bytes);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_req_write_multiple_registers(ModbusCom *mc, uint8_t transmit_addr, 
    uint16_t start_addr, uint16_t qnt_reg, uint16_t *reg_val)
{
	uint8_t byte_cnt = 2 * qnt_reg;

    uint8_t param_size = sizeof(uint8_t) + sizeof(transmit_addr) 
        + sizeof(start_addr) + sizeof(qnt_reg) + sizeof(byte_cnt) 
        + (sizeof(byte_cnt) * byte_cnt);

    uint8_t *pdu_data = malloc(sizeof(uint8_t) * param_size);
    uint16_t pdu_id = 0;

    pdu_data[pdu_id++] = MFC_WriteMultipleRegisters;

    pdu_data[pdu_id++] = HiByteU16(start_addr);
    pdu_data[pdu_id++] = LoByteU16(start_addr);

    pdu_data[pdu_id++] = HiByteU16(qnt_reg);
    pdu_data[pdu_id++] = LoByteU16(qnt_reg);

    pdu_data[pdu_id++] = byte_cnt;

    for(int i = 0; i < qnt_reg; i++)
    {
        pdu_data[pdu_id++] = HiByteU16(reg_val[i]);
        pdu_data[pdu_id++] = LoByteU16(reg_val[i]);
    }

    uint8_t pdu_size = pdu_id;
    modbus_build_request(mc, transmit_addr, pdu_data, pdu_size);

    free(pdu_data);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_req_report_server_id(ModbusCom *mc, uint8_t transmit_addr)
{
    uint8_t pdu_data[] = 
    {   
        MFC_ReportServerID
    };
    
    uint8_t pdu_size = ArraySize(pdu_data);

    modbus_build_request(mc, transmit_addr, pdu_data, pdu_size);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_req_read_file_record(ModbusCom *mc, uint8_t transmit_addr
    /* TODO */)
{
    /**
     * TODO 
     */

    //modbus_build_request(mc, transmit_addr, MFC_ReadFileRecord, pdu_data, 
    //   pdu_size);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_req_write_file_record(ModbusCom *mc, uint8_t transmit_addr
    /* TODO */)
{
    /**
     * TODO 
     */

    //modbus_build_request(mc, transmit_addr, MFC_WriteFileRecord, pdu_data, 
    //  pdu_size);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_req_mask_write_register(ModbusCom *mc, uint8_t transmit_addr,
    uint16_t ref_addres, uint16_t and_mask, uint16_t or_mask)
{
    uint8_t pdu_data[] = 
    {       
        MFC_MaskWriteRegister,

        HiByteU16(ref_addres),
        LoByteU16(ref_addres),

        HiByteU16(and_mask),
        LoByteU16(and_mask),

        HiByteU16(or_mask),
        LoByteU16(or_mask)
    };
    
    uint8_t pdu_size = ArraySize(pdu_data);

    modbus_build_request(mc, transmit_addr, pdu_data, pdu_size);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_req_read_write_multiple_registers(ModbusCom *mc, 
    uint8_t transmit_addr, uint16_t r_start_addr, uint16_t r_qnt_reg, 
    uint16_t w_start_addr, uint16_t w_qnt_reg, uint8_t w_byte_cnt, 
    uint8_t *w_reg_val)
{
    uint8_t param_size = sizeof(uint8_t) + sizeof(transmit_addr) 
        + sizeof(r_start_addr) + sizeof(r_qnt_reg) 
        + sizeof(w_start_addr) + sizeof(w_qnt_reg) 
        + sizeof(w_byte_cnt) + (sizeof(w_byte_cnt) * w_byte_cnt);

    uint8_t *pdu_data = malloc(sizeof(uint8_t) * param_size);
    uint16_t pdu_id = 0;

    pdu_data[pdu_id++] = MFC_ReadWriteMultipleRegisters;

    pdu_data[pdu_id++] = HiByteU16(r_start_addr);
    pdu_data[pdu_id++] = LoByteU16(r_start_addr);

    pdu_data[pdu_id++] = HiByteU16(r_qnt_reg);
    pdu_data[pdu_id++] = LoByteU16(r_qnt_reg);

    pdu_data[pdu_id++] = HiByteU16(w_start_addr);
    pdu_data[pdu_id++] = LoByteU16(w_start_addr);

    pdu_data[pdu_id++] = HiByteU16(w_qnt_reg);
    pdu_data[pdu_id++] = LoByteU16(w_qnt_reg);

    pdu_data[pdu_id++] = w_byte_cnt;

    for(int i = 0; i < w_byte_cnt; i++)
    {
        pdu_data[pdu_id+i] = w_reg_val[i];
    }
    
    uint8_t pdu_size = ArraySize(pdu_data);

    modbus_build_request(mc, transmit_addr, pdu_data, pdu_size);
    
    free(pdu_data);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_req_read_fifo_queue(ModbusCom *mc, uint8_t transmit_addr,
    uint16_t fifo_ptr_addr)
{
    uint8_t pdu_data[] = 
    {       
        MFC_ReadFIFOQueue,

        HiByteU16(fifo_ptr_addr),
        LoByteU16(fifo_ptr_addr)
    };

    uint8_t pdu_size = ArraySize(pdu_data);

    modbus_build_request(mc, transmit_addr, pdu_data, pdu_size);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void modbus_build_request(ModbusCom *mc, uint8_t transmit_addr, 
    uint8_t *pdu_data, uint8_t pdu_size)    
{
    mc->adu[MFI_TransmitAddr] = transmit_addr;

    if(pdu_size > 0)
    {
        for(int i = 0; i < pdu_size; i++)
        {
            mc->adu[MFI_PDUStart + i] = pdu_data[i];
        }
    }

    mc->adu_size = 1 + pdu_size;    

    uint16_t crc = modbus_crc_calculate(mc->adu, mc->adu_size);

    mc->adu[mc->adu_size] = LoByteU16(crc);
    mc->adu[mc->adu_size+1] = HiByteU16(crc);

    mc->adu_size += 2;

    #ifdef MODBUS_DEBUG
        MODBUS_DEBUG_CLBK("----------------------------\n");
        MODBUS_DEBUG_CLBK("modbus_build_request()\n");
        MODBUS_DEBUG_CLBK("mc->pdu[] = { ");
        for(int j = 0; j < mc->adu_size; j++)
        {
            MODBUS_DEBUG_CLBK("%#04X", mc->adu[j]);
            if(j < mc->adu_size-1)
                MODBUS_DEBUG_CLBK(", ");
        }
        MODBUS_DEBUG_CLBK(" } \n");
        MODBUS_DEBUG_CLBK("mc->pdu_size = %d\n", mc->adu_size);
        MODBUS_DEBUG_CLBK("---------------------------- ");
    #endif
}
