#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include"modbus/modbus_util.h"
#include"modbus/modbus_exc_func.h"
#include"modbus/modbus_rsp_func.h"
#include"modbus/modbus_types.h"
#include"modbus/modbus_data_model_func.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void modbus_clear_recv_data(ModbusCom *mc);

static 
ModbusInitializeStatus_t modbus_check_functions_configuration(ModbusCom *mc);

static bool modbus_is_coil_function_supported(ModbusCom *mc);

static bool modbus_is_contact_function_supported(ModbusCom *mc);

static bool modbus_is_hreg_function_supported(ModbusCom *mc);

static bool modbus_is_ireg_function_supported(ModbusCom *mc);

static void modbus_clear_adu_data(ModbusCom *mc);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusCom* modbus_create(void)
{
	return (ModbusCom*)malloc(sizeof(ModbusCom));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_initialize(ModbusCom* mc, ModbusNodeRole_t role, uint8_t node_addr)
{
    mc->supported_func_a = NULL;
    mc->supported_func_cnt = 0;

    mc->receive_flag = MRF_NoData;
    mc->rec_buff_size = 0;
    mc->adu_size = 0;

    mc->ir_a = NULL;
    mc->ir_size = 0;

    mc->hr_a = NULL;
    mc->hr_size = 0;

    mc->doc_a = NULL;
    mc->doc_size = 0;

    mc->dic_a = NULL;
    mc->dic_size = 0;

    mc->send_data_clbk = NULL;

    mc->device_addr = node_addr;
    modbus_set_node_role(mc, role);

    mc->is_init = true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusExceptionState_t modbus_set_send_data_callback(ModbusCom *mc, 
    send_data_callback clbk)
{
    if(mc == NULL || clbk == NULL)
    {
        return MES_Illegal;
    }

    mc->send_data_clbk = clbk;
    return MES_Correct;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusExceptionState_t modbus_set_supported_functions(ModbusCom *mc, 
    const uint8_t *func_array, uint8_t func_cnt)
{
    if(mc == NULL || func_array == NULL || func_cnt == 0)
    {
        return MES_Illegal;
    }

    mc->supported_func_a = func_array;
    mc->supported_func_cnt = func_cnt;
    return MES_Correct;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusExceptionState_t modbus_assign_coils_data(ModbusCom *mc, 
    uint8_t* data, uint16_t size)
{
    if(size >= MODBUS_COILS_ADDR_MIN && size <= MODBUS_COILS_ADDR_MAX)
    {
        mc->doc_a = data;
        mc->doc_size = size;

        return MES_Correct;
    }

    return MES_Illegal;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusExceptionState_t modbus_assign_contacts_data(ModbusCom *mc, 
    uint8_t* data, uint16_t size)
{
    if(size >= MODBUS_CONTACTS_ADDR_MIN && size <= MODBUS_CONTACTS_ADDR_MAX)
    {
        mc->dic_a = data;
        mc->dic_size = size;

        return MES_Correct;
    }

    return MES_Illegal;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusExceptionState_t modbus_assign_hregisters_data(ModbusCom *mc, 
    uint16_t* data, uint16_t size)
{
    if(size >= MODBUS_HREG_ADDR_MIN && size <= MODBUS_HREG_ADDR_MAX)
    {
        mc->hr_a = data;
        mc->hr_size = size;

        return MES_Correct;
    }

    return MES_Illegal;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusExceptionState_t modbus_assign_inregisters_data(ModbusCom *mc,
    uint16_t* data, uint16_t size)
{
    if(size >= MODBUS_IREG_ADDR_MIN && size <= MODBUS_IREG_ADDR_MAX)
    {
        mc->ir_a = data;
        mc->ir_size = size;

        return MES_Correct;
    }

    return MES_Illegal;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
/**
 * @brief   Check Illegal Data Address Exception in receive request frame for all
 *          supported functions with required condition
 * @param   mc: pointer to ModbucCom strucutre
 * @param   role: role for modbus node device
 * @retval  Modbus Exception State
 */
ModbusExceptionState_t modbus_set_node_role(ModbusCom *mc, 
    ModbusNodeRole_t role)
{
    if(role != MNR_Master && role != MNR_Slave)
    {
        mc->role = MNR_NoSelected;
        return MES_Illegal;
    }

    mc->role = role;
    return MES_Correct;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusInitializeStatus_t modbus_check_initialized_status(ModbusCom *mc)
{
    if(mc == NULL || mc->is_init == false)
    {
        return MIS_ErrNotInitialized;
    }

    if(mc->role == MNR_NoSelected)
    {
        return MIS_ErrRoleNotSelected;
    }

    if(mc->send_data_clbk == NULL)
    {
        return MIS_ErrSendDataClbk;        
    }

    if(mc->role == MNR_Master)
    {
        return MIS_Initialized;
    }

    return modbus_check_functions_configuration(mc);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_send_data(ModbusCom *mc)
{
    mc->send_data_clbk(mc->adu, mc->adu_size);
//    MODBUS_DEBUG_CLBK("recv %d = { ", mc->rec_buff_size);
//    for(int i = 0; i < mc->rec_buff_size; i++)
//    	MODBUS_DEBUG_CLBK("%#04X ", mc->rec_buff[i]);
//    MODBUS_DEBUG_CLBK(" } ");
//    MODBUS_DEBUG_CLBK("send %d = { ", mc->adu_size);
//    for(int i = 0; i < mc->adu_size; i++)
//    	MODBUS_DEBUG_CLBK("%#04X ", mc->adu[i]);
//    MODBUS_DEBUG_CLBK(" } ");
    modbus_clear_adu_data(mc);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_receive_data(ModbusCom *mc, uint8_t c_in)
{
    if(mc->receive_flag != MRF_ReceiveData)
    {
        mc->rec_buff[mc->rec_buff_size++] = c_in;
    }    
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_data_handler(ModbusCom *mc)
{
    if(mc->receive_flag == MRF_NoData)
    {
        return ;
    }
    else if(mc->receive_flag == MRF_ReceiveData)
    {
        #ifdef MODBUS_DEBUG
                MODBUS_DEBUG_CLBK("----------------------------\n");
                MODBUS_DEBUG_CLBK("modbus_slave_data_handler()\n");
                MODBUS_DEBUG_CLBK("receive data { ");
                for(int i = 0; i < mc->rec_buff_size; i++)
                {
                    MODBUS_DEBUG_CLBK("%#04X", mc->rec_buff[i]);
                    if(i < mc->rec_buff_size-1)
                    {
                        MODBUS_DEBUG_CLBK(", ");
                    }                    
                }
                MODBUS_DEBUG_CLBK(" }\n");
        #endif

        if(modbus_exc_is_crc_correct(mc->rec_buff, mc->rec_buff_size) 
            == MES_Correct)
        {
        	switch(mc->role)
        	{
        		case MNR_Master:                    
        			modbus_rsp_handler(mc);
        			break;
        		case MNR_Slave:
        			modbus_req_handler(mc);
        			break;
        		default:
        			break;
        	}            
            modbus_send_data(mc);      		
        }
        else
        {
            #ifdef MODBUS_DEBUG
                MODBUS_DEBUG_CLBK("----------------------------\n");
                MODBUS_DEBUG_CLBK("modbus_slave_data_handler()\n");
                MODBUS_DEBUG_CLBK("crc error in receive data { ");
                for(int i = 0; i < mc->rec_buff_size; i++)
                {
                    MODBUS_DEBUG_CLBK("%#04X", mc->rec_buff[i]);
                    if(i < mc->rec_buff_size-1)
                    {
                        MODBUS_DEBUG_CLBK(", ");
                    }                    
                }
                MODBUS_DEBUG_CLBK(" }\n");
            #endif
        }

        modbus_clear_recv_data(mc);
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_req_handler(ModbusCom *mc)
{
    ModbusFunctionCode_t func_code = ModbusFrameFuncCode(mc);
    ModbusExceptionCode_t exc_code = modbus_valid_request(mc);

    if(exc_code != MEC_None)
    {           
        modbus_rsp_exception(mc, func_code, exc_code);

        #ifdef MODBUS_DEBUG
            MODBUS_DEBUG_CLBK("----------------------------\n");
            MODBUS_DEBUG_CLBK("modbus_req_handler()\n");
            MODBUS_DEBUG_CLBK("catch exception in func %d! exc_code = %d\n", 
                func_code, exc_code);
        #endif
    }
    else
    {
        switch(func_code)
        {
            case MFC_ReadCoils: 
                modbus_rsp_read_coils(mc); 
                break;
            case MFC_ReadDiscreteInputs:
                modbus_rsp_read_discrete_inputs(mc);
                break;
            case MFC_ReadHoldingRegisters: 
                modbus_rsp_read_holding_registers(mc); 
                break;
            case MFC_ReadInputRegister: 
                modbus_rsp_read_input_registers(mc); 
                break;
            case MFC_WriteSingleCoil: 
                modbus_rsp_write_single_coil(mc); 
                break;
            case MFC_WriteSingleRegister: 
                modbus_rsp_write_single_register(mc); 
                break;
            case MFC_WriteMultipleCoils: 
                modbus_rsp_write_multiple_coils(mc); 
                break;
            case MFC_WriteMultipleRegisters: 
                modbus_rsp_write_multiple_registers(mc); 
                break;
            default:
                break;
        }

        #ifdef MODBUS_DEBUG
            MODBUS_DEBUG_CLBK("----------------------------\n");
            MODBUS_DEBUG_CLBK("modbus_req_handler()\n");
            MODBUS_DEBUG_CLBK("func %d from request handled!\n", func_code);
        #endif
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_rsp_handler(ModbusCom *mc)
{

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

uint16_t modbus_crc_calculate(uint8_t *adu, uint16_t adu_size)
{
    uint16_t crc = 0xFFFF;

    for(int i = 0; i < adu_size; i++)
    {
        crc ^= adu[i];

        for(int j = 0; j < 8; j++)
        {
            if((crc & 0x0001) != 0)
            {
                crc >>= 1;
                crc ^= 0xA001;
            }
            else
            {
                crc >>= 1;
            }
        }
    }
    
    #ifdef MODBUS_DEBUG
        MODBUS_DEBUG_CLBK("----------------------------\n");
        MODBUS_DEBUG_CLBK("modbus_crc_calculate()\n");
        MODBUS_DEBUG_CLBK("crc = %#06X\n", crc);
        MODBUS_DEBUG_CLBK("lo_crc hi_crc = %#04X %#04X\n", LoByteU16(crc), HiByteU16(crc));
    #endif

    return crc;
} 


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static 
ModbusInitializeStatus_t modbus_check_functions_configuration(ModbusCom *mc)
{
    if(modbus_is_coil_function_supported(mc) == true)
    {
        if(mc->doc_a == NULL || mc->doc_size == 0)
        {
            return MIS_ErrFuncDOC;
        }
    }
    if(modbus_is_contact_function_supported(mc) == true)
    {
        if(mc->dic_a == NULL || mc->dic_size == 0)
        {
            return MIS_ErrFuncDIC;
        }
    }
    if(modbus_is_hreg_function_supported(mc) == true)
    {
        if(mc->hr_a == NULL || mc->hr_size == 0)
        {
            return MIS_ErrFuncHReg;
        }
    }   
    if(modbus_is_ireg_function_supported(mc) == true)
    {
        if(mc->ir_a == NULL || mc->ir_size == 0)
        {
            return MIS_ErrFuncIReg;
        }
    }    

    return MIS_Initialized;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool modbus_is_coil_function_supported(ModbusCom *mc)
{
    for(int i = 0; i < mc->supported_func_cnt; i++)
    {
        uint8_t fc = mc->supported_func_a[i];

        if(fc == MFC_ReadCoils || fc == MFC_WriteSingleCoil 
            || fc == MFC_WriteMultipleCoils)
        {
            return true;
        }
    }

    return false;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool modbus_is_contact_function_supported(ModbusCom *mc)
{
    for(int i = 0; i < mc->supported_func_cnt; i++)
    {
        uint8_t fc = mc->supported_func_a[i];

        if(fc == MFC_ReadDiscreteInputs)
        {
            return true;
        }
    }

    return false;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool modbus_is_hreg_function_supported(ModbusCom *mc)
{
    for(int i = 0; i < mc->supported_func_cnt; i++)
    {
        uint8_t fc = mc->supported_func_a[i];

        if(fc == MFC_ReadHoldingRegisters || fc == MFC_WriteSingleRegister 
            || fc == MFC_WriteMultipleRegisters)
        {
            return true;
        }
    }

    return false;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool modbus_is_ireg_function_supported(ModbusCom *mc)
{
    for(int i = 0; i < mc->supported_func_cnt; i++)
    {
        uint8_t fc = mc->supported_func_a[i];

        if(fc == MFC_ReadInputRegister)
        {
            return true;
        }
    }

    return false;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void modbus_clear_recv_data(ModbusCom *mc)
{
    memset(mc->rec_buff, 0, MODBUS_ADU_SIZE);
    mc->rec_buff_size = 0;
    mc->receive_flag = MRF_NoData;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void modbus_clear_adu_data(ModbusCom *mc)
{
	memset(mc->adu, 0, MODBUS_ADU_SIZE);
	mc->adu_size = 0;
}

/*
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

uint8_t* modbus_get_coils_contacts_status(uint16_t start_addr, uint16_t qnt_c,
    uint8_t byte_cnt, uint8_t *coils_contacts_a)
{
    uint8_t *data = malloc(sizeof(uint8_t) * byte_cnt);

    uint16_t c_nr;
    uint16_t c_addr;

    for(int byte_nr = 0; byte_nr < byte_cnt; byte_nr++)
    {
        for(int bit_pos = 0; bit_pos < 8; bit_pos++)
        {
            c_nr = (byte_nr*8) + bit_pos;
            c_addr = start_addr + c_nr;

            if(c_nr >= qnt_c)
            {   
                data[byte_nr] = ResetBitInByte(data[byte_nr], bit_pos);    

                #ifdef MODBUS_DEBUG
                    MODBUS_DEBUG_CLBK("filledd rest of byte with 0 value");
                    uint8_t tmp = (data[byte_nr] & (1 << bit_pos)) >> bit_pos; 

                    MODBUS_DEBUG_CLBK(" | bn = %d | c_nr = %d | c_addr = %d | tmp = %d\n", 
                        byte_nr, c_nr, c_addr, tmp);                   
                #endif                
            }
            else
            {
                uint8_t cv = modbus_get_single_coil_contact_status(c_addr, 
                    coils_contacts_a);
                
                data[byte_nr] = ChangeBitValueInByte(data[byte_nr], cv, 
                    bit_pos);

                #ifdef MODBUS_DEBUG
                    MODBUS_DEBUG_CLBK("get single coil value\n");
                    MODBUS_DEBUG_CLBK("bn = %d | c_nr = %d | c_addr = %d | cv = %d\n", 
                        byte_nr, c_nr, c_addr, cv);
                #endif   
            }
        }
    }

    return data;
}
*/
