#include<stdio.h>
#include<string.h>

#include"modbus/modbus_recv_func.h"
#include"modbus/modbus_exc_func.h"
#include"modbus/modbus_util.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static ModbusReceiveStatus_t recv_read_response(ModbusCom *mc, 
    ModbusFunctionCode_t fc, void *data, uint8_t data_size);

static ModbusReceiveStatus_t recv_write_response(ModbusCom *mc,
    ModbusFunctionCode_t func_code);

static void modbus_clear_adu_data(ModbusCom *mc);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusReceiveStatus_t modbus_recv_read_coils(ModbusCom *mc, uint8_t **data,
    uint8_t data_size)
{
	ModbusReceiveStatus_t ret_code =
			recv_read_response(mc, MFC_ReadCoils, data, data_size);

    return ret_code;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusReceiveStatus_t modbus_recv_read_discrete_inputs(ModbusCom *mc, 
    uint8_t **data, uint8_t data_size)
{
	ModbusReceiveStatus_t ret_code =
			recv_read_response(mc, MFC_ReadDiscreteInputs, data, data_size);

	return ret_code;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusReceiveStatus_t modbus_recv_read_holding_registers(ModbusCom *mc, 
    uint16_t **data, uint8_t data_size)
{
	ModbusReceiveStatus_t ret_code =
			recv_read_response(mc, MFC_ReadHoldingRegisters, data, data_size);

	return ret_code;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusReceiveStatus_t modbus_recv_read_input_registers(ModbusCom *mc, 
    uint16_t **data, uint8_t data_size)
{
	ModbusReceiveStatus_t ret_code =
			recv_read_response(mc, MFC_ReadInputRegister, data, data_size);

	return ret_code;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusReceiveStatus_t modbus_recv_write_single_coil(ModbusCom *mc)
{
	ModbusReceiveStatus_t ret_code =
			recv_write_response(mc, MFC_WriteSingleCoil);

	return ret_code;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusReceiveStatus_t modbus_recv_write_single_register(ModbusCom *mc)
{
	ModbusReceiveStatus_t ret_code =
			recv_write_response(mc, MFC_WriteSingleRegister);

	return ret_code;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusReceiveStatus_t modbus_recv_write_multiple_coils(ModbusCom *mc)
{
	ModbusReceiveStatus_t ret_code =
			recv_write_response(mc, MFC_WriteMultipleCoils);

	return ret_code;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusReceiveStatus_t modbus_recv_write_multiple_registers(ModbusCom *mc)
{
	ModbusReceiveStatus_t ret_code =
			recv_write_response(mc, MFC_WriteMultipleRegisters);

	return ret_code;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static ModbusReceiveStatus_t recv_read_response(ModbusCom *mc, 
    ModbusFunctionCode_t fc, void *data, uint8_t data_size)
{
	if(mc->receive_flag == MRF_NoData)
	{
		return MRS_WaitForResponse;
	}

    #ifdef MODBUS_DEBUG
        MODBUS_DEBUG_CLBK("----------------------------\n");
        MODBUS_DEBUG_CLBK("recv_read_response()\n");
        for(int i = 0; i < mc->rec_buff_size; i++)
        {
            MODBUS_DEBUG_CLBK("%#04X", mc->rec_buff[i]);
            if(i < mc->rec_buff_size-1)
            {
                MODBUS_DEBUG_CLBK(", ");
            }                    
        }
        MODBUS_DEBUG_CLBK(" }\n");    
    #endif
    
    if(modbus_exc_is_timeout_respond(mc) == MES_Illegal)
    {
        #ifdef MODBUS_DEBUG
            MODBUS_DEBUG_CLBK("MRS_ErrorTimeout\n");  
        #endif
        return MRS_ErrorTimeout;
    }

    if(modbus_exc_is_crc_correct(mc->rec_buff, mc->rec_buff_size) 
        == MES_Correct)
    {
        //check if receive respond is from correct modbus function
        if(mc->rec_buff[MFI_FuncCode] != fc)
        {
            modbus_exc_check_rsp_exception(mc);
            #ifdef MODBUS_DEBUG
                MODBUS_DEBUG_CLBK("MRS_ErrorFuncCode\n");  
            #endif
            return MRS_ErrorFuncCode;
        }

        uint8_t byte_cnt = ModbusRspByteCntByte(mc);

        if(fc == MFC_ReadCoils || fc == MFC_ReadDiscreteInputs)
        {
            if(DataSizeToByteCnt(data_size) != byte_cnt)
            {
                #ifdef MODBUS_DEBUG
                    MODBUS_DEBUG_CLBK("data_size %d | size %d | byte_cnt %d\n",
                        data_size, DataSizeToByteCnt(data_size), byte_cnt); 
                    MODBUS_DEBUG_CLBK("MRS_ErrorDataSizeIO\n");
                #endif
                return MRS_ErrorDataSize;
            }       

            uint8_t **p_data = ((uint8_t**)data);
            int byte_nr = 0;
            int bit_pos = 0;            

            //assign receive data to array
            for(int data_id = 0; data_id < data_size; data_id++)
            {
                uint8_t array_id = bit_pos + (byte_nr*8);
                uint8_t val = ReadBitFromByte(mc->rec_buff[MFI_RspValuesByte+byte_nr], bit_pos);
                *(p_data[array_id]) = val;

                bit_pos++;
                if(bit_pos==8)
                {
                    byte_nr++;
                    bit_pos=0;
                }                
            }
        }
        else if(fc == MFC_ReadHoldingRegisters || fc == MFC_ReadInputRegister)
        {
            if((data_size*2) != byte_cnt)
            {
                #ifdef MODBUS_DEBUG
                    MODBUS_DEBUG_CLBK("data_size %d | byte_cnt %d\n",
                        data_size, byte_cnt); 
                    MODBUS_DEBUG_CLBK("MRS_ErrorDataSizeRegs\n");
                #endif
                return MRS_ErrorDataSize;
            }       

            uint16_t **p_data = ((uint16_t**)data);

            //assign receive data to array
            for(int byte_nr = 0; byte_nr < byte_cnt/2; byte_nr++)
            {
                *p_data[byte_nr] = (mc->rec_buff[MFI_RspValuesByte+(byte_nr*2)] << 8)
                    | mc->rec_buff[MFI_RspValuesByte+(byte_nr*2)+1];
            }
        }
    }
    else
    {
        #ifdef MODBUS_DEBUG
            MODBUS_DEBUG_CLBK("MRS_ErrorCRC\n");  
        #endif
        return MRS_ErrorCRC;
    }
    return MRS_CorrectRespond;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static ModbusReceiveStatus_t recv_write_response(ModbusCom *mc,
    ModbusFunctionCode_t func_code)
{
    #ifdef MODBUS_DEBUG
        MODBUS_DEBUG_CLBK("----------------------------\n");
        MODBUS_DEBUG_CLBK("recv_write_response()\nmc->rec_buff[] = ");
        for(int i = 0; i < mc->rec_buff_size; i++)
        {
            MODBUS_DEBUG_CLBK("%#04X", mc->rec_buff[i]);
            if(i < mc->rec_buff_size-1)
            {
                MODBUS_DEBUG_CLBK(", ");
            }                    
        }
        MODBUS_DEBUG_CLBK(" }\n");    
        MODBUS_DEBUG_CLBK("mc->rec_buff_size = %d\n", mc->rec_buff_size);
    #endif
    
    if(modbus_exc_is_timeout_respond(mc) == MES_Illegal)
    {
        #ifdef MODBUS_DEBUG
            MODBUS_DEBUG_CLBK("MRS_ErrorTimeout\n");  
        #endif
        return MRS_ErrorTimeout;
    }

    if(modbus_exc_is_crc_correct(mc->rec_buff, mc->rec_buff_size) 
        == MES_Correct)
    {
        ModbusFunctionCode_t fc = mc->rec_buff[MFI_FuncCode];
        //check if receive respond is from correct modbus function
        if(fc != func_code)
        {
            modbus_exc_check_rsp_exception(mc);
            #ifdef MODBUS_DEBUG
                MODBUS_DEBUG_CLBK("MRS_ErrorFuncCode\n");  
            #endif
            return MRS_ErrorFuncCode;
        }
        if(fc == MFC_WriteMultipleRegisters || fc == MFC_WriteMultipleCoils)
        {
            return modbus_exc_is_correct_rsp_for_mult_write(mc);
        }
        else
        {
            if(modbus_exc_is_rsp_is_req_echo(mc) == MES_Correct)
            {
                #ifdef MODBUS_DEBUG
                    MODBUS_DEBUG_CLBK("MRS_CorrectRespond\n");  
                #endif
                return MRS_CorrectRespond;
            }
            else
            {
                #ifdef MODBUS_DEBUG
                    MODBUS_DEBUG_CLBK("MRS_ErrorDataValue\n");  
                #endif
                return MRS_ErrorDataValue;
            }
        }
        
    }
    else
    {
        #ifdef MODBUS_DEBUG
            MODBUS_DEBUG_CLBK("MRS_ErrorCRC\n");  
        #endif
        return MRS_ErrorCRC;
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void modbus_clear_adu_data(ModbusCom *mc)
{
    memset(mc->adu, 0, MODBUS_ADU_SIZE);
    mc->adu_size = 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

