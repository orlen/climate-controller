#include "modbus/modbus_port.h"
#include "modbus/modbus_conf.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                        E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusCom *mc = NULL;

uint8_t modbus_cin[1];

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                        S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static TIM_HandleTypeDef modbus_frame_timer;
UART_HandleTypeDef modbus_uart;

static const uint8_t modbus_supported_func[] =
{
    MFC_ReadCoils,
    MFC_ReadDiscreteInputs,
    MFC_ReadHoldingRegisters,
    MFC_ReadInputRegister,
    MFC_WriteSingleCoil,
    MFC_WriteSingleRegister,
    MFC_WriteMultipleCoils,
    MFC_WriteMultipleRegisters
};
static uint8_t msf_cnt = ArraySize(modbus_supported_func);

uint8_t doc_a[MODBUS_DOC_SIZE] = { 0 };

uint8_t dic_a[MODBUS_DIC_SIZE] = { 0 };

uint16_t ir_a[MODBUS_IREG_SIZE] = { 0 };

uint16_t hr_a[MODBUS_HREG_SIZE] = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void modbus_frame_timer_init(void);

static void modbus_uart_init(void);

static void modbus_led_transmit_init(void);

static void modbus_send_callback(uint8_t *data, uint16_t size);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_communication_initialize(void)
{
	mc = modbus_create();
	modbus_initialize(mc, MODBUS_NODE_ROLE, MODBUS_NODE_ADDR);
	modbus_set_send_data_callback(mc, modbus_send_callback);

	if(MODBUS_NODE_ROLE == MNR_Slave)
	{
		modbus_set_supported_functions(mc, modbus_supported_func, msf_cnt);

		modbus_assign_coils_data(mc, doc_a, MODBUS_DOC_SIZE);
		modbus_assign_contacts_data(mc, dic_a, MODBUS_DIC_SIZE);
		modbus_assign_inregisters_data(mc, ir_a, MODBUS_IREG_SIZE);
		modbus_assign_hregisters_data(mc, hr_a, MODBUS_HREG_SIZE);
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_transmit_enable(void)
{
	modbus_frame_timer_init();
	modbus_uart_init();
	modbus_led_transmit_init();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_tim_period_elapsed(TIM_HandleTypeDef *htim)
{
	if(htim->Instance == MODBUS_TIM_INSTANCE)
	{
		#ifdef MODBUS_LED_RX_ENABLE
		HAL_GPIO_WritePin(MODBUS_LED_RX_OUT, 0);
		#endif
		__HAL_TIM_CLEAR_IT(&modbus_frame_timer, TIM_IT_UPDATE);
		HAL_TIM_Base_Stop_IT(&modbus_frame_timer);
		mc->receive_flag = MRF_ReceiveData;
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_usart_rx_cplt(UART_HandleTypeDef *huart)
{
	if(huart->Instance == MODBUS_USART_INSTANCE)
	{
		#ifdef MODBUS_LED_RX_ENABLE
		HAL_GPIO_WritePin(MODBUS_LED_RX_OUT, 1);
		#endif
		HAL_TIM_Base_Stop_IT(&modbus_frame_timer);
		HAL_UART_Receive_IT(&modbus_uart, modbus_cin, 1);
		modbus_receive_data(mc, modbus_cin[0]);
		HAL_TIM_Base_Start_IT(&modbus_frame_timer);
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//              P E R I P H E R I A L S   I R Q   H A N D L E R S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void MODBUS_USART_HANDLER(void)
{
	HAL_UART_IRQHandler(&modbus_uart);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void MODBUS_TIM_HANDLER(void)
{
	HAL_TIM_IRQHandler(&modbus_frame_timer);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void modbus_frame_timer_init(void)
{
	MODBUS_TIM_CLK_EN();
	modbus_frame_timer.Instance = MODBUS_TIM_INSTANCE;
	modbus_frame_timer.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
	modbus_frame_timer.Init.CounterMode = TIM_COUNTERMODE_UP;
	modbus_frame_timer.Init.Prescaler = 61750-1;
	modbus_frame_timer.Init.Period = 2-1;
	modbus_frame_timer.Init.ClockDivision = 0;

	HAL_TIM_Base_Init(&modbus_frame_timer);
	__HAL_TIM_CLEAR_IT(&modbus_frame_timer, TIM_IT_UPDATE);

	HAL_NVIC_SetPriority(MODBUS_TIM_IRQn, 3, 0);
	HAL_NVIC_EnableIRQ(MODBUS_TIM_IRQn);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void modbus_uart_init(void)
{
  modbus_uart.Instance = MODBUS_USART_INSTANCE;
  modbus_uart.Init.BaudRate = MODBUS_USART_BAUDRATE;
  modbus_uart.Init.WordLength = UART_WORDLENGTH_8B;
  modbus_uart.Init.StopBits = MODBUS_USART_STOPBITS;
  modbus_uart.Init.Parity = MODBUS_USART_PARITY;
  modbus_uart.Init.Mode = UART_MODE_TX_RX;
  modbus_uart.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  modbus_uart.Init.OverSampling = UART_OVERSAMPLING_16;
  modbus_uart.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  modbus_uart.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&modbus_uart) != HAL_OK)
  {
    ;//Error_Handler();
  }
  HAL_UART_Receive_IT(&modbus_uart, modbus_cin, 1);
  HAL_NVIC_SetPriority(MODBUS_USART_IRQn, 3, 0);
  HAL_NVIC_EnableIRQ(MODBUS_USART_IRQn);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void modbus_send_callback(uint8_t *data, uint16_t size)
{
	HAL_UART_Transmit(&modbus_uart, data, size, 100);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void modbus_led_transmit_init(void)
{
	#ifdef MODBUS_LED_RX_ENABLE

		GPIO_InitTypeDef gpio;
		gpio.Pin = MODBUS_LED_RX_PIN;
		gpio.Mode = GPIO_MODE_OUTPUT_PP;
		HAL_GPIO_Init(MODBUS_LED_RX_PORT, &gpio);

	#endif
}
