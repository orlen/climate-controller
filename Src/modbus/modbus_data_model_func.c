#include "modbus/modbus_data_model_func.h"
#include "modbus/modbus_util.h"

#include <stdlib.h>
#include <stdio.h>


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

uint8_t* modbus_get_coils_contacts_status(uint16_t start_addr, uint16_t qnt_c,
    uint8_t byte_cnt, uint8_t *coils_contacts_a)
{
    uint8_t *data = malloc(sizeof(uint8_t) * byte_cnt);

    uint16_t c_nr;
    uint16_t c_addr;

    for(int byte_nr = 0; byte_nr < byte_cnt; byte_nr++)
    {
        for(int bit_pos = 0; bit_pos < 8; bit_pos++)
        {
            c_nr = (byte_nr*8) + bit_pos;
            c_addr = start_addr + c_nr;

            if(c_nr >= qnt_c)
            {   
                data[byte_nr] = ResetBitInByte(data[byte_nr], bit_pos);    

                #ifdef MODBUS_DEBUG
                    MODBUS_DEBUG_CLBK("filledd rest of byte with 0 value");
                    uint8_t tmp = (data[byte_nr] & (1 << bit_pos)) >> bit_pos; 

                    MODBUS_DEBUG_CLBK(" | bn = %d | c_nr = %d | c_addr = %d | tmp = %d\n", 
                        byte_nr, c_nr, c_addr, tmp);                   
                #endif                
            }
            else
            {
                uint8_t cv = modbus_get_single_coil_contact_status(c_addr, 
                    coils_contacts_a);
                
                data[byte_nr] = ChangeBitValueInByte(data[byte_nr], cv, 
                    bit_pos);

                #ifdef MODBUS_DEBUG
                    MODBUS_DEBUG_CLBK("get single coil value\n");
                    MODBUS_DEBUG_CLBK("bn = %d | c_nr = %d | c_addr = %d | cv = %d\n", 
                        byte_nr, c_nr, c_addr, cv);
                #endif   
            }
        }
    }
    return data;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

uint8_t modbus_get_single_coil_contact_status(uint16_t addr, 
    uint8_t *coils_contacts_a)
{
    return coils_contacts_a[addr];
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_set_single_coil_status(uint16_t out_addr, 
    uint8_t *coils_contacts_a, uint16_t val)
{
    coils_contacts_a[out_addr] = val == 0x0000 ? 0 : 1;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_set_multiple_coils_status(uint16_t out_addr, 
    uint16_t qnt_c, uint8_t *coils_contacts_a, uint8_t *val)
{
    for(int coil_nr = 0; coil_nr < qnt_c; coil_nr++)
    {
        uint8_t v_byte_nr = (coil_nr)/8;
        uint8_t bit_pos = coil_nr%8;
        uint8_t reg_addr = out_addr + coil_nr;

        uint8_t bit_value = (val[v_byte_nr] & (1 << bit_pos)) >> bit_pos;
        coils_contacts_a[reg_addr] = bit_value;

        #ifdef MODBUS_DEBUG
            MODBUS_DEBUG_CLBK("----------------------------\n");
            MODBUS_DEBUG_CLBK("modbus_set_multiple_coils_status()\n");
            MODBUS_DEBUG_CLBK("coil_nr %d | ", coil_nr);
            MODBUS_DEBUG_CLBK("v_byte_nr %d | ", v_byte_nr);
            MODBUS_DEBUG_CLBK("bit_pos %d | ", bit_pos);
            MODBUS_DEBUG_CLBK("bit_value %d | ", bit_value);
            MODBUS_DEBUG_CLBK("reg_addr %d | ", reg_addr);
            MODBUS_DEBUG_CLBK("coils_contacts_a %d | \n", coils_contacts_a[reg_addr]);
        #endif
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

uint8_t* modbus_get_registers_value(uint16_t start_addr, uint8_t byte_cnt, 
    uint16_t *registers_a)
{
    uint8_t *reg_data = malloc(sizeof(uint8_t) * byte_cnt);
    
    for(int byte_nr = 0, reg_addr = start_addr; 
        byte_nr < byte_cnt; byte_nr+=2, reg_addr++)
    {
        reg_data[byte_nr] = HiByteU16(registers_a[reg_addr]);
        reg_data[byte_nr+1] = LoByteU16(registers_a[reg_addr]);
    }

    return reg_data;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_set_single_register_value(uint16_t start_addr, 
    uint16_t *registers_a, uint16_t val)
{
    registers_a[start_addr] = val;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_set_multiple_registers_value(uint16_t start_addr, uint8_t byte_cnt, 
    uint16_t *registers_a, uint16_t *val)
{
    for(int byte_nr = 0; byte_nr < byte_cnt; byte_nr++)
    {
        registers_a[start_addr+byte_nr] = val[byte_nr];
    }
}
