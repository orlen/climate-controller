#include<stdio.h>
#include<stdlib.h>

#include"modbus/modbus_rsp_func.h"
#include"modbus/modbus_util.h"
#include"modbus/modbus_data_model_func.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void modbus_build_response(ModbusCom *mc, uint8_t *pdu_data, uint8_t pdu_size);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_rsp_read_coils(ModbusCom *mc)
{   
    uint16_t start_addr = ModbusFrameAddrByte(mc); 
    uint16_t qnt_coils = ModbusFrameQntByte(mc);
    uint8_t byte_cnt = ModbusCoilsContactsByteCnt(qnt_coils);

    uint8_t *coils_data = modbus_get_coils_contacts_status(start_addr,
        qnt_coils, byte_cnt, mc->doc_a);

    uint16_t pdu_size = sizeof(uint8_t) * (byte_cnt + 2); //data+byte_cnt+func_code
    uint8_t *pdu_data = malloc(sizeof(uint8_t) * pdu_size);
    uint8_t pdu_id = 0;

    pdu_data[pdu_id++] = MFC_ReadCoils;
    pdu_data[pdu_id++] = byte_cnt;

    for(int i = 0; i < byte_cnt; i++)
    {
        pdu_data[pdu_id+i] = coils_data[i];
    }

    modbus_build_response(mc, pdu_data, pdu_size);

    free(pdu_data);
    free(coils_data);

    #ifdef MODBUS_DEBUG
        MODBUS_DEBUG_CLBK("----------------------------\n");
        MODBUS_DEBUG_CLBK("modbus_rsp_read_coils()\n");    
        MODBUS_DEBUG_CLBK("qnt_coils = %d\n", qnt_coils);   
        MODBUS_DEBUG_CLBK("start_addr = %d\n", start_addr);
        MODBUS_DEBUG_CLBK("byte_cnt = %d\n", byte_cnt);
    #endif
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_rsp_read_discrete_inputs(ModbusCom *mc)
{
    uint16_t start_addr = ModbusFrameAddrByte(mc);    
    uint16_t qnt_inputs = ModbusFrameQntByte(mc);
    uint8_t byte_cnt = ModbusCoilsContactsByteCnt(qnt_inputs);

    uint8_t *inputs_data = modbus_get_coils_contacts_status(start_addr,
        qnt_inputs, byte_cnt, mc->dic_a);

    uint16_t pdu_size = sizeof(uint8_t) * (byte_cnt + 2); //data+byte_cnt+func_code
    uint8_t *pdu_data = malloc(sizeof(uint8_t) * pdu_size);
    uint8_t pdu_id = 0;

    pdu_data[pdu_id++] = MFC_ReadDiscreteInputs;
    pdu_data[pdu_id++] = byte_cnt;
    
    for(int i = 0; i < byte_cnt; i++)
    {
        pdu_data[pdu_id+i] = inputs_data[i];
    }

    modbus_build_response(mc, pdu_data, pdu_size);

    free(pdu_data);
    free(inputs_data);

    #ifdef MODBUS_DEBUG
        MODBUS_DEBUG_CLBK("----------------------------\n");
        MODBUS_DEBUG_CLBK("modbus_rsp_read_discrete_inputs()\n");    
        MODBUS_DEBUG_CLBK("qnt_inputs = %d\n", qnt_inputs);   
        MODBUS_DEBUG_CLBK("start_addr = %d\n", start_addr);
        MODBUS_DEBUG_CLBK("byte_cnt = %d\n", byte_cnt);
    #endif
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_rsp_read_holding_registers(ModbusCom *mc)
{   
    uint16_t start_addr = ModbusFrameAddrByte(mc); 
    uint16_t qnt_registers = ModbusFrameQntByte(mc);
    uint8_t byte_cnt = ModbusRegistersByteCnt(qnt_registers);

    uint8_t *reg_data = modbus_get_registers_value(start_addr, byte_cnt, 
        mc->hr_a);

    uint8_t pdu_size = sizeof(uint8_t) * (byte_cnt + 2); //data+byte_cnt+func_code
    uint8_t *pdu_data = malloc(sizeof(uint8_t) * pdu_size);
    uint8_t pdu_id = 0;

    pdu_data[pdu_id++] = MFC_ReadHoldingRegisters;
    pdu_data[pdu_id++] = byte_cnt;

    for(int i = 0; i < byte_cnt; i++)
    {
        pdu_data[pdu_id+i] = reg_data[i];
    }

    modbus_build_response(mc, pdu_data, pdu_size);

    free(pdu_data);
    free(reg_data);

    #ifdef MODBUS_DEBUG
        MODBUS_DEBUG_CLBK("----------------------------\n");
        MODBUS_DEBUG_CLBK("modbus_rsp_read_holding_registers()\n");    
        MODBUS_DEBUG_CLBK("qnt_registers = %d\n", qnt_registers);   
        MODBUS_DEBUG_CLBK("start_addr = %d\n", start_addr);
        MODBUS_DEBUG_CLBK("byte_cnt = %d\n", byte_cnt);
    #endif
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_rsp_read_input_registers(ModbusCom *mc)
{   
    uint16_t start_addr = ModbusFrameAddrByte(mc); 
    uint16_t qnt_in_registers = ModbusFrameQntByte(mc);
    uint8_t byte_cnt = ModbusRegistersByteCnt(qnt_in_registers);

    uint8_t *reg_data = modbus_get_registers_value(start_addr, byte_cnt, 
        mc->ir_a);

    uint8_t pdu_size = sizeof(uint8_t) * (byte_cnt + 2); //data+byte_cnt+func_code
    uint8_t *pdu_data = malloc(sizeof(uint8_t) * pdu_size);
    uint8_t pdu_id = 0;

    pdu_data[pdu_id++] = MFC_ReadInputRegister;
    pdu_data[pdu_id++] = byte_cnt;
    
    for(int i = 0; i < byte_cnt; i++)
    {
        pdu_data[pdu_id+i] = reg_data[i];
    }

    modbus_build_response(mc, pdu_data, pdu_size);

    free(pdu_data);
    free(reg_data);
    
    #ifdef MODBUS_DEBUG
        MODBUS_DEBUG_CLBK("----------------------------\n");
        MODBUS_DEBUG_CLBK("modbus_rsp_read_input_registers()\n");    
        MODBUS_DEBUG_CLBK("qnt_in_registers = %d\n", qnt_in_registers);   
        MODBUS_DEBUG_CLBK("start_addr = %d\n", start_addr);
        MODBUS_DEBUG_CLBK("byte_cnt = %d\n", byte_cnt);
    #endif
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_rsp_write_single_coil(ModbusCom *mc)
{
    uint16_t out_addr = ModbusFrameAddrByte(mc);
    uint16_t out_val = ModbusFrameSingleValByte(mc);

    modbus_set_single_coil_status(out_addr, mc->doc_a, out_val);

    uint8_t pdu_data[] = 
    {   
        MFC_WriteSingleCoil,

        HiByteU16(out_addr),
        LoByteU16(out_addr),

        HiByteU16(out_val),
        LoByteU16(out_val)
    };

    uint8_t pdu_size = ArraySize(pdu_data);

    modbus_build_response(mc, pdu_data, pdu_size);   

    #ifdef MODBUS_DEBUG
        MODBUS_DEBUG_CLBK("----------------------------\n");
        MODBUS_DEBUG_CLBK("modbus_rsp_write_single_coil()\n");  
        MODBUS_DEBUG_CLBK("out_addr = %d\n", out_addr);  
        MODBUS_DEBUG_CLBK("out_val = %d\n", out_val);   
    #endif 
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_rsp_write_single_register(ModbusCom *mc)
{
    uint16_t reg_addr = ModbusFrameAddrByte(mc);
    uint16_t reg_val = ModbusFrameSingleValByte(mc);

    modbus_set_single_register_value(reg_addr, mc->hr_a, reg_val);

    uint8_t pdu_data[] = 
    {   
        MFC_WriteSingleRegister,

        HiByteU16(reg_addr),
        LoByteU16(reg_addr),

        HiByteU16(reg_val),
        LoByteU16(reg_val)
    };

    uint8_t pdu_size = ArraySize(pdu_data);

    modbus_build_response(mc, pdu_data, pdu_size); 

    #ifdef MODBUS_DEBUG
        MODBUS_DEBUG_CLBK("----------------------------\n");
        MODBUS_DEBUG_CLBK("modbus_rsp_write_single_register()\n");  
        MODBUS_DEBUG_CLBK("reg_addr = %d\n", reg_addr);  
        MODBUS_DEBUG_CLBK("reg_val = %d\n", reg_val);   
    #endif 
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_rsp_write_multiple_coils(ModbusCom *mc)
{
    uint16_t start_addr = ModbusFrameAddrByte(mc);
    uint16_t qnt_out = ModbusFrameQntByte(mc);
    uint8_t byte_cnt = ModbusFrameByteCntByte(mc);
    uint8_t *out_val = malloc(sizeof(uint8_t) * byte_cnt);

    for(int byte_nr = 0; byte_nr < byte_cnt; byte_nr++)
    {
        out_val[byte_nr] = ModbusFrameMulCoilValByte(mc, byte_nr);
    }
    
    modbus_set_multiple_coils_status(start_addr, qnt_out, mc->doc_a,
        out_val);

    uint8_t pdu_data[] = 
    {   
        MFC_WriteMultipleCoils,

        HiByteU16(start_addr),
        LoByteU16(start_addr),

        HiByteU16(qnt_out),
        LoByteU16(qnt_out)
    };

    uint8_t pdu_size = ArraySize(pdu_data);

    modbus_build_response(mc, pdu_data, pdu_size);
    
    free(out_val);
    
    #ifdef MODBUS_DEBUG
        MODBUS_DEBUG_CLBK("----------------------------\n");
        MODBUS_DEBUG_CLBK("modbus_rsp_write_multiple_coils()\n");  
        MODBUS_DEBUG_CLBK("start_addr = %d\n", start_addr);  
        MODBUS_DEBUG_CLBK("qnt_out = %d\n", qnt_out);  
        MODBUS_DEBUG_CLBK("byte_cnt = %d\n", byte_cnt); 
        for(int i = 0; i < byte_cnt; i++)
        {
            MODBUS_DEBUG_CLBK("out_val[%d] = %d\n", i, out_val[i]); 
        } 
    #endif 
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_rsp_write_multiple_registers(ModbusCom *mc)
{
    uint16_t start_addr = ModbusFrameAddrByte(mc);
    uint16_t qnt_reg = ModbusFrameQntByte(mc);
    uint8_t byte_cnt = ModbusFrameByteCntByte(mc);
    uint16_t *out_val = malloc(sizeof(uint16_t) * byte_cnt);

MODBUS_DEBUG_CLBK("sa %d, q %d, b %d\n", start_addr, qnt_reg, byte_cnt);
    for(int val_id = 0, byte_nr = 0; byte_nr < byte_cnt; val_id++, byte_nr+=2)
    {        
        out_val[val_id] = ModbusFrameMulRegValByte(mc, byte_nr);
        MODBUS_DEBUG_CLBK("ov[%d] %d \n", byte_nr, out_val[byte_nr]);
    }
    
    modbus_set_multiple_registers_value(start_addr, qnt_reg, mc->hr_a,
        out_val);

    uint8_t pdu_data[] = 
    {   
        MFC_WriteMultipleRegisters,

        HiByteU16(start_addr),
        LoByteU16(start_addr),

        HiByteU16(qnt_reg),
        LoByteU16(qnt_reg)
    };

    uint8_t pdu_size = ArraySize(pdu_data);

    modbus_build_response(mc, pdu_data, pdu_size);

    free(out_val);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_rsp_exception(ModbusCom *mc, ModbusFunctionCode_t fc,
     ModbusExceptionCode_t exc_code)
{
    uint8_t pdu[] = 
    {
        ModbusSetErrorCode(fc),
        exc_code
    };

    uint8_t pdu_size = ArraySize(pdu);
    
    modbus_build_response(mc, pdu, pdu_size);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void modbus_build_response(ModbusCom *mc, uint8_t *pdu_data, uint8_t pdu_size)
{    
    mc->adu[MFI_TransmitAddr] = mc->device_addr;

    if(pdu_size > 0)
    {
        for(int i = 0; i < pdu_size; i++)
        {
            mc->adu[MFI_PDUStart + i] = pdu_data[i];
        }
    }

    mc->adu_size = 1 + pdu_size;

    uint16_t crc = modbus_crc_calculate(mc->adu, mc->adu_size);
    
    mc->adu[mc->adu_size] = LoByteU16(crc);
    mc->adu[mc->adu_size+1] = HiByteU16(crc);

    mc->adu_size += 2;

    #ifdef MODBUS_DEBUG
        MODBUS_DEBUG_CLBK("----------------------------\n");
        MODBUS_DEBUG_CLBK("modbus_build_response()\n");
        MODBUS_DEBUG_CLBK("mc->pdu[] = { ");
        for(int j = 0; j < mc->adu_size; j++)
        {
            MODBUS_DEBUG_CLBK("%#04X", mc->adu[j]);
            if(j < mc->adu_size-1)
                MODBUS_DEBUG_CLBK(", ");
        }
        MODBUS_DEBUG_CLBK(" } \n");
        MODBUS_DEBUG_CLBK("mc->pdu_size = %d\n", mc->adu_size);
    #endif    
}
