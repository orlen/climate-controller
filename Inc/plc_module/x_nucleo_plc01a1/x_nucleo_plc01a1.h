/**
  ******************************************************************************
  * @file    X_NUCLEO_PLC01A1.h
  * @author  CL
  * @version V1.1.0
  * @date    25-April-2016
  * @brief   X-NUCLEO-PLC01A1_Application
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/** @defgroup X-NUCLEO-PLC01A1_Exported_variables     X-NUCLEO-PLC01A1 Exported variables
  * @{
  * @brief Exported variables 
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __X_NUCLEO_PLC01A1_H
#define __X_NUCLEO_PLC01A1_H
    
/* Includes ------------------------------------------------------------------*/
//#include "cube_hal.h"

#include "relay.h"
#include "current_limiter.h"

#include "vni8200xp.h"
#include "clt01_38s.h"

#define ALL_OUTPUTS_OFF         0x00
#define RESET_FLAG              0

   
uint8_t BSP_Signal_Mirror(uint8_t);
uint8_t BSP_Output_Freeze(uint8_t, uint8_t, uint32_t*);
uint8_t BSP_Output_Regroup(uint8_t);
uint8_t BSP_Inputs_Sum(uint8_t);
uint8_t BSP_Output_ON(uint8_t);
uint8_t BSP_Output_OFF(uint8_t);
uint8_t BSP_Inputs_AND(uint8_t, uint8_t);
uint8_t BSP_Inputs_OR(uint8_t, uint8_t);
uint8_t BSP_Inputs_NOT(uint8_t);
uint8_t BSP_Inputs_XOR(uint8_t, uint8_t);
RELAY_StatusTypeDef BSP_GetRelayStatus(uint8_t*);
CURRENT_LIMITER_StatusTypeDef BSP_GetCurrentLimiterStatus(uint8_t*);
void BSP_OuputCycling_Init(uint16_t, uint8_t);

void BSP_RELAY_Reset(plc_module_t *plc);
void BSP_RELAY_EN_Out(plc_module_t *plc);
void BSP_RELAY_DIS_Out(plc_module_t *plc);

uint8_t* BSP_RELAY_SetOutputs(plc_module_t *plc, uint8_t*);
uint8_t* BSP_CURRENT_LIMITER_Read(plc_module_t *plc);

RELAY_StatusTypeDef BSP_Relay_Init(plc_module_t *plc);
CURRENT_LIMITER_StatusTypeDef BSP_CurrentLimiter_Init(plc_module_t *plc);


#endif

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
