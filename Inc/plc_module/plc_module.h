#ifndef __PLC_MODULE_H
#define __PLC_MODULE_H

#include "stm32f7xx_hal.h"
#include "stm32f7xx_hal_spi.h"

#include "plc_module/x_nucleo_plc01a1/relay.h"
#include "plc_module/x_nucleo_plc01a1/current_limiter.h"


#define PLC_PRINT	service_print

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef enum _plc_state_e
{
	PLC_STATE_OK,
	PLC_STATE_CURRENT_ERROR,
	PLC_STATE_RELAY_ERROR

} plc_state_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef enum _output_nr
{
	RELAY_OUT_1 = 0,
	RELAY_OUT_2,
	RELAY_OUT_3,
	RELAY_OUT_4,
	RELAY_OUT_5,
	RELAY_OUT_6,
	RELAY_OUT_7,
	RELAY_OUT_8,

} relay_out_n_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef enum _input_nr
{
	CURRENT_IN_1 = 0,
	CURRENT_IN_2,
	CURRENT_IN_3,
	CURRENT_IN_4,
	CURRENT_IN_5,
	CURRENT_IN_6,
	CURRENT_IN_7,
	CURRENT_IN_8,

} current_in_n_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef void (*open_spi_pinout_callback)(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef struct _plc_module_s
{
	SPI_HandleTypeDef *hspi;

	GPIO_TypeDef	*relay_en_out_port;
	uint16_t 		relay_en_out_pin;

	GPIO_TypeDef	*relay_cs_port;
	uint16_t 		relay_cs_pin;

	GPIO_TypeDef	*current_limiter_cs_port;
	uint16_t 		current_limiter_cs_pin;

	uint8_t inputs_states;

	uint8_t outputs_states;
	uint8_t set_outputs_states;

	open_spi_pinout_callback open_spi_io_clbk;

	CURRENT_LIMITER_StatusTypeDef current_state;

	uint8_t relay_init;
	RELAY_StatusTypeDef relay_state;

	plc_state_t state;

} plc_module_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

plc_module_t *plc_module_create(void);

void plc_module_init(plc_module_t *plc, SPI_TypeDef *spix);

void plc_module_communication_init(plc_module_t *plc);

void plc_module_set_outputs(plc_module_t *plc, uint8_t output_states);

void plc_module_set_single_output(plc_module_t *plc, relay_out_n_t outn, 
								  uint8_t on_off);

RELAY_StatusTypeDef plc_module_send_outputs(plc_module_t *plc);

RELAY_StatusTypeDef plc_module_set_and_send_outputs(plc_module_t *plc,
													uint8_t output_states);

RELAY_StatusTypeDef plc_module_set_and_send_single_output(plc_module_t *plc,
										   relay_out_n_t outn, uint8_t on_off);

uint8_t* plc_module_read_inputs_handler(plc_module_t *plc);

uint8_t plc_module_get_input_state(plc_module_t *plc, current_in_n_t inn);

uint8_t plc_module_get_output_state(plc_module_t *plc, relay_out_n_t outn);

void plc_module_mirror_signals_handler(plc_module_t *plc);

void plc_module_print_inputs(plc_module_t *plc);

void plc_module_print_states(plc_module_t *plc);

void plc_module_output_cycling(plc_module_t *plc);

void plc_module_reset_module(plc_module_t *plc);


#endif /* __PLC_MODULE_H */
