#ifndef __MDMC_VALUES_H
#define __MDMC_VALUES_H

#include "modbus_datamodel/modbus_datamodel_addr.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

#define MCMD_PLC_RESET			0x0001

#define MCMD_CHRG_PWR_DISABLE	0x0001
#define MCMD_CHRG_PWR_ENABLE	0x0002

#define MCMD_CHRG_LOG_DISABLE	0x0001
#define MCMD_CHRG_LOG_ENABLE	0x0002

#define MCMD_IRLOCK_DISABLE	0x0001
#define MCMD_IRLOCK_ENABLE		0x0002

#endif /* __MDMC_VALUES_H */
