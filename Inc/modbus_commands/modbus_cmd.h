#ifndef __MODBUS_CMD_H
#define __MODBUS_CMD_H


#include <stdint.h>


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef struct _modbus_cmd_s modbus_cmd_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef void (*modbus_cmd_handler_clbk)(modbus_cmd_t*);

typedef void (*modbus_cmd_init_clbk)(modbus_cmd_t*);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

struct _modbus_cmd_s
{
    uint16_t cmd_resp;
    uint16_t *cmd_val;

    modbus_cmd_init_clbk init_clbk;
    modbus_cmd_handler_clbk handler_clbk;
    
};


#endif /* __MODBUS_CMD_H */