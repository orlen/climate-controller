#ifndef __MODBUS_COMMANDS_H
#define __MODBUS_COMMANDS_H

#include "modbus_commands/cmd_list/mcmd_list.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_commands_handler(void);


#endif /* __MODBUS_COMMANDS_H */