#ifndef __MODBUS_DATAMODEL_ADDR_H
#define __MODBUS_DATAMODEL_ADDR_H


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             D I S C R E T E   O U T P U T S   R E G I S T E R S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define MDOC_CLIM_HANG_HEAT_RELAY	0x0000
#define MDOC_CLIM_HANG_AC_RELAY	0x0001
#define MDOC_CLIM_HANG_FAN_RELAY	0x0002
#define MDOC_CLIM_ROOF_HEAT_RELAY	0x0003
#define MDOC_CHARG_POWER_RELAY		0x0004
#define MDOC_CHARG_LOGIC_RELAY		0x0005
#define MDOC_IRLOCK_RELAY			0x0006

#define MDOC_SIZE					0x0007

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             D I S C R E T E   I N P U T S   R E G I S T E R S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define MDIC_PRESSURE_MIN_ALARM	0x0000
#define MDIC_PRESSURE_MAX_ALARM	0x0001

#define MDIC_SIZE					0x0002

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         I N P U T   R E G I S T E R S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define MIR_SYSTEM_READY			0x0000
#define MIR_SYSLOG					0x0001
#define MIR_CLIM_HANG_UNL_TEMP		0x0002
#define MIR_CLIM_HANG_UNL_HUMD		0x0003
#define MIR_CLIM_HANG_UPL_TEMP		0x0004
#define MIR_CLIM_HANG_UPL_HUMD		0x0005
#define MIR_CLIM_ROOF_L_TEMP		0x0006
#define MIR_CLIM_ROOF_R_TEMP		0x0007
#define MIR_PRESSURE_STATE			0x0008
#define MIR_CHARG_POWER_STATE		0x0009
#define MIR_CHARG_LOGIC_STATE		0x000A
#define MIR_IRLOCK_STATE			0x000B

#define MIR_SIZE					0x000C

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                       H O L D I N G   R E G I S T E R S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define MHR_SYSLOG_ACK				0x0001
#define MHR_CLIM_HANG_SET_TEMP		0x0002
#define MHR_CLIM_HANG_SET_HUMD		0x0003
#define MHR_CLIM_ROOF_SET_TEMP		0x0004
#define MHR_CMD_CHARG_POWER		0x0009
#define MHR_CMD_CHARG_LOGIC		0x000A
#define MHR_CMD_IRLOCK				0x000B
#define MHR_CMD_PLC_RESET			0x000C

#define MHR_SIZE					0x000D


#endif /* __MODBUS_DATAMODEL_ADDR_H */
