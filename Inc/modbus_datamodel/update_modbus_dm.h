#ifndef __UPDATE_MODBUS_DM_H
#define __UPDATE_MODBUS_DM_H


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void update_modbus_data_model();


#endif /* __UPDATE_MODBUS_DM_H */
