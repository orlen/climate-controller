#ifndef __LOG_EVENTS_CONTROLLER_H
#define __LOG_EVENTS_CONTROLLER_H

#include "log_events/log_event.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void log_events_controller_handler();

uint16_t log_events_get_reported_log_code();

uint16_t log_events_check_reported_log_event_ack(uint16_t ack_val);


#endif /* __LOG_EVENTS_CONTROLLER_H */