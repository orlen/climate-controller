#ifndef __LOG_EVENT_H
#define __LOG_EVENT_H

#include <stdint.h>
#include <stdbool.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef struct _log_event_s log_event_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef enum _log_event_e
{
    LOG_EVENT_STATE_IDLE,
    LOG_EVENT_STATE_TRIGGERED,
    LOG_EVENT_STATE_REPORTED,
    LOG_EVENT_STATE_RECEIVED

} log_event_state_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef bool (*log_condition_callback)(log_event_t *);

typedef void (*log_ackhandler_callback)(log_event_t *);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

struct _log_event_s
{    
    uint16_t code;

    log_event_state_t state;
    
    log_condition_callback condition_clbk;
    log_ackhandler_callback ackhandler_clbk;

};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


#endif /* __LOG_EVENT_H */
