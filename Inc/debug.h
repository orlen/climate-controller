/**
  ******************************************************************************
  * @file           : debug.h
  * @brief          : Header file for debug file
  * @author			: Mateusz Strojek
  ******************************************************************************
  *
  * COPYRIGHT(c) 2019 Cervi Robotics
  */

#ifndef __DEBUG_H
#define __DEBUG_H

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include <stm32f7xx_hal.h>

extern UART_HandleTypeDef huart3;

void service_print(const char *fmt, ...);
void MX_USART3_UART_Init(void);

#endif /* __DEBUG_H */
