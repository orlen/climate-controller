#ifndef __PERIPHERALS_SETTINGS_H
#define __PERIPHERALS_SETTINGS_H


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

// P L C   M O D U L E

#define PLC_MODULE_CNT 	1
#define PLC_TIMER_INTERVAL_MS	50

// P L C   1

#define PLC1_SPIX   SPI1

#define PLC1_ID		0

#define PLC1_CURRENT_CS_PORT GPIOD
#define PLC1_CURRENT_CS_PIN	 GPIO_PIN_15

#define PLC1_RELAY_CS_PORT GPIOD
#define PLC1_RELAY_CS_PIN  GPIO_PIN_14

#define PLC1_RELAY_EN_PORT GPIOE
#define PLC1_RELAY_EN_PIN  GPIO_PIN_9

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
// 							P L C   P E R I P H E R A L S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

// H A N G A R   C L I M A T E   R E L A Y S

#define HANGAR_HEAT_RELAY_PLC_ID		PLC1_ID
#define HANGAR_HEAT_RELAY_PLC_OUT		RELAY_OUT_1
#define HANGAR_HEAT_RELAY_INIT_STATE	OFF

#define HANGAR_AC_RELAY_PLC_ID 			PLC1_ID
#define HANGAR_AC_RELAY_PLC_OUT 		RELAY_OUT_2
#define HANGAR_AC_RELAY_INIT_STATE		OFF

#define HANGAR_FAN_RELAY_PLC_ID			PLC1_ID
#define HANGAR_FAN_RELAY_PLC_OUT		RELAY_OUT_3
#define HANGAR_FAN_RELAY_INIT_STATE		OFF

#define ROOF_HEAT_RELAY_PLC_ID			PLC1_ID
#define ROOF_HEAT_RELAY_PLC_OUT			RELAY_OUT_4
#define ROOF_HEAT_RELAY_INIT_STATE		OFF

// S U P P O R T   R E L A Y S

#define CHARGER_POWER_RELAY_PLC_ID		PLC1_ID
#define CHARGER_POWER_RELAY_PLC_OUT		RELAY_OUT_5
#define CHARGER_POWER_RELAY_INIT_STATE	ON

#define CHARGER_LOGIC_RELAY_PLC_ID		PLC1_ID
#define CHARGER_LOGIC_RELAY_PLC_OUT		RELAY_OUT_6
#define CHARGER_LOGIC_RELAY_INIT_STATE	ON

#define IRLOCK_RELAY_PLC_ID				PLC1_ID
#define IRLOCK_RELAY_PLC_OUT			RELAY_OUT_7
#define IRLOCK_RELAY_INIT_STATE			OFF

// P R E S S U R E   S E N S O R   O U T P U T S

#define PRESSURE_MIN_ALARM_PLC_ID		PLC1_ID
#define PRESSURE_MIN_ALARM_PLC_IN		CURRENT_IN_1

#define PRESSURE_MAX_ALARM_PLC_ID		PLC1_ID
#define PRESSURE_MAX_ALARM_PLC_IN		CURRENT_IN_2

// A D C   P E R I P H E R A L S

#define ADC_CHANNELS 2

// L E D   L O O P

#define LED_LOOP_PORT  GPIOB
#define LED_LOOP_PIN   GPIO_PIN_0

// L O G   E V E N T   S I G N A L

#define LOGEVT_PORT     GPIOB
#define LOGEVT_PIN      GPIO_PIN_7


#endif /* __PERIPHERALS_SETTINGS_H */

