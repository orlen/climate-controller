#ifndef __INITIALIZE_PERIPHERIALS_H
#define __INITIALIZE_PERIPHERIALS_H

#include "stm32f767xx.h"

#include "initialize_peripherials/peripherals_settings.h"

#include "temp_sensors/thermistor.h"
#include "temp_sensors/am2301.h"
#include "plc_module/plc_module.h"
#include "modbus/modbus_port.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern plc_module_t *plc[PLC_MODULE_CNT];

extern TIM_HandleTypeDef plc_timer;

extern uint16_t adc_values[ADC_CHANNELS];

extern am2301_t *am2301_unl;
extern am2301_t *am2301_upl;

extern thermistor_t *thermistor_rsl;
extern thermistor_t *thermistor_rsr;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void initialize_plc(void);

void initialize_plc_timer();

void initialize_support_peripherals_state(void);

void initialize_adc_dma(void);

void initialize_led_loop(void);

void initialize_led_log_event(void);

void initialize_temp_sensors(void);


#endif /* __INITIALIZE_PERIPHERIALS_H */
