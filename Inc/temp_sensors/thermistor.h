#ifndef __THERMISTOR_H
#define __THERMISTOR_H

#include <stdint.h>


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef struct _thermistor_s
{
    uint16_t *adc_val;

    double adc_res;
    double beta_value;
    double nom_resistance;
    double nom_temperature;
    double ref_resistance;

} thermistor_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

thermistor_t *thermistor_create();

double thermistor_get_temp_K(thermistor_t *th);

double thermistor_get_temp_C(thermistor_t *th);


#endif /* __THERMISTOR_H */
