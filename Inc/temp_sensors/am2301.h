#ifndef __AM2301_H
#define __AM2301_H

#include "stm32f7xx_hal.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef enum _am2301_state_e
{
    am2301_state_ok,
    am2301_comm_error,
	am2301_read_data_error,
    am2301_parity_error

} am2301_state_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef struct _am2301_s
{
    GPIO_TypeDef *gpiox;
    uint16_t gpio_pin_x;

    int16_t temperature;
    uint16_t humidity;

    am2301_state_t state;

} am2301_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

am2301_t *am2301_create();

int am2301_read_data(am2301_t *am);


#endif /* __AM2301_H */
