#ifndef __MODBUS_CONF_H
#define __MODBUS_CONF_H

#include "modbus_datamodel/modbus_datamodel_addr.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

#define MODBUS_RTU

#define MODBUS_NODE_ROLE	MNR_Slave /* { MNR_Slave ; MNR_Master } */
#define MODBUS_NODE_ADDR	0x03

#define MODBUS_DOC_SIZE		MDOC_SIZE
#define MODBUS_DIC_SIZE		MDIC_SIZE
#define MODBUS_IREG_SIZE	MIR_SIZE
#define MODBUS_HREG_SIZE	MHR_SIZE

//#define MODBUS_DEBUG
#define MODBUS_DEBUG_CLBK   service_print


#endif /* __MODBUS_CONF_H */
