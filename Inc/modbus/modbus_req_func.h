#ifndef __MODBUS_REQ_FUNC_H
#define __MODBUS_REQ_FUNC_H

#include "modbus_types.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_req_read_coils(ModbusCom *mc, uint8_t transmit_addr, 
    uint16_t start_addr, uint16_t qnt_coils);

void modbus_req_read_discrete_inputs(ModbusCom *mc, uint8_t transmit_addr, 
    uint16_t start_addr, uint16_t qnt_inputs);

void modbus_req_read_holding_registers(ModbusCom *mc, uint8_t transmit_addr, 
    uint16_t start_addr, uint16_t qnt_registers);

void modbus_req_read_input_registers(ModbusCom *mc, uint8_t transmit_addr, 
    uint16_t start_addr, uint16_t qnt_in_registers);

void modbus_req_write_single_coil(ModbusCom *mc, uint8_t transmit_addr, 
    uint16_t out_addr, uint16_t out_val);

void modbus_req_write_single_register(ModbusCom *mc, uint8_t transmit_addr, 
    uint16_t reg_addr, uint16_t reg_val);

void modbus_req_read_exception_status(ModbusCom *mc, uint8_t transmit_addr);

void modbus_req_diagnostics(ModbusCom *mc, uint8_t transmit_addr, 
    ModbusDiagnosticsSubfunction_t mds, uint16_t data);

void modbus_req_get_comm_event_counter(ModbusCom *mc, uint8_t transmit_addr);

void modbus_req_get_comm_event_log(ModbusCom *mc, uint8_t transmit_addr);

void modbus_req_write_multiple_coils(ModbusCom *mc, uint8_t transmit_addr, 
    uint16_t start_addr, uint16_t qnt_out, uint8_t *out_val);

void modbus_req_write_multiple_registers(ModbusCom *mc, uint8_t transmit_addr, 
    uint16_t start_addr, uint16_t qnt_reg, uint16_t *reg_val);

void modbus_req_report_server_id(ModbusCom *mc, uint8_t transmit_addr);

void modbus_req_read_file_record(ModbusCom *mc, uint8_t transmit_addr
    /* TODO */);

void modbus_req_write_file_record(ModbusCom *mc, uint8_t transmit_addr
    /* TODO */);

void modbus_req_mask_write_register(ModbusCom *mc, uint8_t transmit_addr,
    uint16_t ref_addres, uint16_t and_mask, uint16_t or_mask);

void modbus_req_read_write_multiple_registers(ModbusCom *mc, 
    uint8_t transmit_addr, uint16_t r_start_addr, uint16_t r_qnt_reg, 
    uint16_t w_start_addr, uint16_t w_qnt_reg, uint8_t w_byte_cnt, 
    uint8_t *w_reg_val);

void modbus_req_read_fifo_queue(ModbusCom *mc, uint8_t transmit_addr,
    uint16_t fifo_ptr_addr);


#endif /* __MODBUS_REQ_FUNC_H */
