#ifndef MODBUS_RECV_FUNC__
#define MODBUS_RECV_FUNC__

#include"modbus_types.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusReceiveStatus_t modbus_recv_read_coils(ModbusCom *mc, uint8_t **data,
    uint8_t data_size);

ModbusReceiveStatus_t modbus_recv_read_discrete_inputs(ModbusCom *mc,
    uint8_t **data, uint8_t data_size);

ModbusReceiveStatus_t modbus_recv_read_holding_registers(ModbusCom *mc, 
    uint16_t **data, uint8_t data_size);

ModbusReceiveStatus_t modbus_recv_read_input_registers(ModbusCom *mc, 
    uint16_t **data, uint8_t data_size);

ModbusReceiveStatus_t modbus_recv_write_single_coil(ModbusCom *mc);

ModbusReceiveStatus_t modbus_recv_write_single_register(ModbusCom *mc);

ModbusReceiveStatus_t modbus_recv_write_multiple_coils(ModbusCom *mc);

ModbusReceiveStatus_t modbus_recv_write_multiple_registers(ModbusCom *mc);


#endif /* MODBUS_RECV_FUNC__ */
