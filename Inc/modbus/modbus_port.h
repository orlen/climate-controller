#ifndef __MODBUS_PORT_H
#define __MODBUS_PORT_H

#include "modbus.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

#define STM32_HAL_DRIVER

#ifdef STM32_HAL_DRIVER
    #include "stm32f7xx_hal.h" //HAL library include

	#define MODBUS_USART_INSTANCE   UART4
	#define MODBUS_USART_BAUDRATE   115200
	#define MODBUS_USART_STOPBITS   UART_STOPBITS_2 /* { 0_5 ; 1 ; 1_5 ; 2 } */
	#define MODBUS_USART_PARITY     UART_PARITY_NONE /* { NONE ; EVEN ; ODD } */

	#define MODBUS_USART_IRQn		UART4_IRQn
	#define MODBUS_USART_HANDLER 	UART4_IRQHandler

	#define MODBUS_TIM_INSTANCE		TIM3
	#define MODBUS_TIM_CLK_EN()		__TIM3_CLK_ENABLE()
	#define MODBUS_TIM_IRQn 		TIM3_IRQn
	#define MODBUS_TIM_HANDLER 		TIM3_IRQHandler

	#define MODBUS_LED_RX_ENABLE

	#ifdef MODBUS_LED_RX_ENABLE
		#define MODBUS_LED_RX_PORT_CLK_EN()	__GPIOB_CLK_ENABLE()
		#define MODBUS_LED_RX_PORT    		GPIOB
		#define MODBUS_LED_RX_PIN     		GPIO_PIN_14
		#define MODBUS_LED_RX_OUT MODBUS_LED_RX_PORT,MODBUS_LED_RX_PIN
	#endif

#endif

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern ModbusCom *mc;

extern UART_HandleTypeDef modbus_uart;

extern uint8_t modbus_cin[1];

extern uint8_t doc_a[MODBUS_DOC_SIZE];

extern uint8_t dic_a[MODBUS_DIC_SIZE];

extern uint16_t ir_a[MODBUS_IREG_SIZE];

extern uint16_t hr_a[MODBUS_HREG_SIZE];

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_communication_initialize(void);

void modbus_transmit_enable(void);

void modbus_usart_rx_cplt(UART_HandleTypeDef *huart);

void modbus_tim_period_elapsed(TIM_HandleTypeDef *htim);


#endif /* __MODBUS_PORT_H */
