#ifndef __MODBUS_DATA_MODEL_FUNC_H
#define __MODBUS_DATA_MODEL_FUNC_H

#include "modbus_types.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

uint8_t* modbus_get_coils_contacts_status(uint16_t start_addr, uint16_t qnt_c,
    uint8_t byte_cnt, uint8_t *coils_contacts_a);

uint8_t modbus_get_single_coil_contact_status(uint16_t addr, 
    uint8_t *coils_contacts_a);

void modbus_set_single_coil_status(uint16_t out_addr, 
    uint8_t *coils_contacts_a, uint16_t val);

void modbus_set_multiple_coils_status(uint16_t out_addr, 
    uint16_t qnt_c, uint8_t *coils_contacts_a, uint8_t *val);

uint8_t* modbus_get_registers_value(uint16_t start_addr, uint8_t byte_cnt, 
    uint16_t *registers_a);

void modbus_set_single_register_value(uint16_t start_addr, 
    uint16_t *registers_a, uint16_t val);

void modbus_set_multiple_registers_value(uint16_t start_addr, uint8_t byte_cnt, 
    uint16_t *registers_a, uint16_t *val);


#endif /* __MODBUS_DATA_MODEL_FUNC_H */
