#ifndef __MODBUS_UTIL_H
#define __MODBUS_UTIL_H

#include"modbus_types.h"
#include"modbus_conf.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define ResetBitInByte(byte, bit_pos) (~(1 << bit_pos) & byte)

#define SetBitInByte(byte, bit_pos) ((1 << bit_pos) | byte)

#define ChangeBitValueInByte(byte, bit_val, bit_pos) ((bit_val == 1) ? \
        SetBitInByte(byte, bit_pos) : ResetBitInByte(byte, bit_pos))

#define ReadBitFromByte(byte, bit_pos) ((byte & (1 << bit_pos)) >> bit_pos)

#define HiByteU16(bytes) (bytes >> 8)
#define LoByteU16(bytes) (bytes & 0x00FF)

#define ArraySize(array) (sizeof(array)/sizeof(array[0]))

#define DataSizeToByteCnt(data_size) (data_size%8 > 0 ? data_size/8+1 : data_size/8)

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusCom* modbus_create(void);

void modbus_initialize(ModbusCom* mc, ModbusNodeRole_t role, uint8_t node_addr);

ModbusExceptionState_t modbus_set_send_data_callback(ModbusCom *mc, 
    send_data_callback clbk);

ModbusExceptionState_t modbus_set_supported_functions(ModbusCom *mc, 
    const uint8_t *func_array, uint8_t func_cnt);

ModbusExceptionState_t modbus_assign_coils_data(ModbusCom *mc, 
    uint8_t* data, uint16_t size);

ModbusExceptionState_t modbus_assign_contacts_data(ModbusCom *mc, 
    uint8_t* data, uint16_t size);

ModbusExceptionState_t modbus_assign_hregisters_data(ModbusCom *mc, 
    uint16_t* data, uint16_t size);

ModbusExceptionState_t modbus_assign_inregisters_data(ModbusCom *mc,
    uint16_t* data, uint16_t size);

ModbusExceptionState_t modbus_set_node_role(ModbusCom *mc, 
    ModbusNodeRole_t role);

ModbusInitializeStatus_t modbus_check_initialized_status(ModbusCom *mc);

void modbus_send_data(ModbusCom *mc);

void modbus_receive_data(ModbusCom *mc, uint8_t c_in); /* TODO */

void modbus_data_handler(ModbusCom *mc);

void modbus_req_handler(ModbusCom *mc);

void modbus_rsp_handler(ModbusCom *mc);

uint16_t modbus_crc_calculate(uint8_t *adu, uint16_t adu_size);


#endif /* __MODBUS_UTIL_H */

