#ifndef __MODBUS_EXC_FUNC_H
#define __MODBUS_EXC_FUNC_H

#include"modbus_types.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

ModbusExceptionCode_t modbus_valid_request(ModbusCom *mc);

ModbusExceptionState_t modbus_exc_is_crc_correct(uint8_t *adu, uint16_t adu_size);

ModbusExceptionState_t modbus_exc_is_illegal_function(ModbusCom *mc, 
    uint8_t func_code);

ModbusExceptionState_t modbus_exc_is_illegal_data_value(ModbusCom *mc, 
    uint8_t func_code);

ModbusExceptionState_t modbus_exc_is_illegal_data_address(ModbusCom *mc, 
    uint8_t func_code);

ModbusExceptionState_t modbus_exc_is_timeout_respond(ModbusCom *mc);

ModbusExceptionState_t modbus_exc_is_rsp_is_req_echo(ModbusCom *mc);

ModbusExceptionState_t modbus_exc_is_correct_rsp_for_mult_write(ModbusCom *mc);

ModbusExceptionState_t modbus_exc_check_rsp_exception(ModbusCom *mc);


#endif /* __MODBUS_EXC_FUNC_H */