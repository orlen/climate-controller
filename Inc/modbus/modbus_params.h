#ifndef __MODBUS_PARAMS_H
#define __MODBUS_PARAMS_H

#include"modbus_conf.h"
#include"modbus_params.h"

#ifdef MODBUS_RTU
    #define FRAME_MAX_SIZE  256
#elif MODBUS_TCP

#else
    #error "Select modbus protocol version!"
#endif

#define MODBUS_BROADCAST_ADDR 0x00
#define MODBUS_ADU_SIZE 256
#define MODBUS_ERROR_CODE 0x80

#define MODBUS_R_COILS_QNT_MIN 0x0001
#define MODBUS_R_COILS_QNT_MAX 0x07D0

#define MODBUS_W_COILS_QNT_MIN 0x0001
#define MODBUS_W_COILS_QNT_MAX 0x07B0

#define MODBUS_COILS_ADDR_MIN 0x0000
#define MODBUS_COILS_ADDR_MAX 0xFFFF

#define MODBUS_COIL_VALUE_HIGH 0xFF00
#define MODBUS_COIL_VALUE_LOW  0x0000

#define MODBUS_CONTACTS_QNT_MIN 0x0001
#define MODBUS_CONTACTS_QNT_MAX 0x07D0

#define MODBUS_CONTACTS_ADDR_MIN 0x0000
#define MODBUS_CONTACTS_ADDR_MAX 0xFFFF

#define MODBUS_R_HREG_QNT_MIN 0x0001
#define MODBUS_R_HREG_QNT_MAX 0x007D

#define MODBUS_W_HREG_QNT_MIN 0x0001
#define MODBUS_W_HREG_QNT_MAX 0x007B

#define MODBUS_HREG_ADDR_MIN 0x0000
#define MODBUS_HREG_ADDR_MAX 0xFFFF

#define MODBUS_HREG_MIN_VAL 0x0000
#define MODBUS_HREG_MAX_VAL 0xFFFF

#define MODBUS_IREG_QNT_MIN 0x0001
#define MODBUS_IREG_QNT_MAX 0x007D

#define MODBUS_IREG_ADDR_MIN 0x0000
#define MODBUS_IREG_ADDR_MAX 0xFFFF

#define MODBUS_SINGLE_WRITE 0x01

#define MODBUS_MULTIPLE_WRITE_RSP_SIZE  8

#define ModbusSetErrorCode(func_code) (MODBUS_ERROR_CODE + func_code)

#define ModbusCrcHiByte(mc) (mc.adu[mc.adu_size - 1])
#define ModbusCrcLoByte(mc) (mc.adu[mc.adu_size - 2])

#define ModbusFrameTransmitAddrByte(mc) (mc->rec_buff[MFI_TransmitAddr])

#define ModbusCoilsContactsByteCnt(qnt_coils) ((qnt_coils % 8 > 0) ? \
        (qnt_coils / 8 + 1) : (qnt_coils/8))

#define ModbusRegistersByteCnt(qnt_registers) (qnt_registers * 2)

#define ModbusFrameFuncCode(mc) (mc->rec_buff[MFI_FuncCode])

#define ModbusFrameAddrByte(mc) (mc->rec_buff[MFI_DataAddrHiByte] << 8 |\
    mc->rec_buff[MFI_DataAddrLoByte])

#define ModbusFrameQntByte(mc) (mc->rec_buff[MFI_DataValHiByte] << 8 |\
    mc->rec_buff[MFI_DataValLoByte])

#define ModbusFrameSingleValByte(mc) (mc->rec_buff[MFI_DataValHiByte] << 8 |\
    mc->rec_buff[MFI_DataValLoByte])

#define ModbusFrameMulCoilValByte(mc, byte_nr) (mc->rec_buff[MFI_OutputsValueBytes+byte_nr])

#define ModbusFrameMulRegValByte(mc, byte_nr) (mc->rec_buff[MFI_OutputsValueBytes+byte_nr] << 8 |\
    mc->rec_buff[MFI_OutputsValueBytes+byte_nr+1])

#define ModbusFrameByteCntByte(mc) (mc->rec_buff[MFI_ByteCntByte])

#define ModbusRspByteCntByte(mc) (mc->rec_buff[MFI_RspByteCnt])

#define ModbusReceiveCrc(adu, adu_size) (adu[adu_size-1] << 8 | adu[adu_size-2])

#endif /* __MODBUS_PARAMS_H */