#ifndef __MODBUS_TYPES_H
#define __MODBUS_TYPES_H

#include<stdint.h>
#include<stdbool.h>

#include"modbus_params.h"

/**
 *  @brief Modbus Function Codes 
 */

typedef enum
{
    /**
     * Public Function Codes Beginning
     */
    MFC_ReadCoils = 0x01,
    MFC_ReadDiscreteInputs = 0x02,
    MFC_ReadHoldingRegisters = 0x03,
    MFC_ReadInputRegister = 0x04,
    MFC_WriteSingleCoil = 0x05,
    MFC_WriteSingleRegister = 0x06,
    MFC_ReadExceptionStatus = 0x07,
    MFC_Diagnostics = 0x08,
    MFC_GetComEventCnt = 0x0B,
    MFC_GetComEventLog = 0x0C,
    MFC_WriteMultipleCoils = 0x0F,
    MFC_WriteMultipleRegisters = 0x10,
    MFC_ReportServerID = 0x11,
    MFC_ReadFileRecord = 0x14,
    MFC_WriteFileRecord = 0x15,
    MFC_MaskWriteRegister = 0x16,
    MFC_ReadWriteMultipleRegisters = 0x17,
    MFC_ReadFIFOQueue = 0x18,
    MFC_ReadDeviceIdentification = 0x2B
    /**
     * Public Function Codes End
     */
    
} ModbusFunctionCode_t; 

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

/**
 * @brief Modbus Exception Codes
 */

typedef enum
{
    MEC_None = 0x00,

    /**
     * The function code received in the query is not an allowable action for 
     * the server. This may be because the function code is only applicable to
     * newer devices, and was not implemented in the unit selected. It could 
     * also indicate that the server is in the wrong state to process a request
     * of this type, for example because it is unconfigured and is being asked
     * to return register values.
     * [http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf]
    */
    MEC_IllegalFunction = 0x01,

    /**
     * The data address received in the query is not an allowable address for 
     * the server. More specifically, the combination of reference number and
     * transfer length is invalid. For a controller with 100 registers, the PDU
     * addresses the first register as 0, and the last one as 99. If a request 
     * is submitted with a starting register address of 96 and a quantity of 
     * registers of 4, then this request will successfully operate 
     * (address-wise at least) on registers 96, 97, 98, 99. If a request is 
     * submitted with a starting register address of 96 and a quantity of 
     * registers of 5, then this request will fail with Exception Code 0x02 
     * “Illegal Data Address” since it attempts to operate on registers 
     * 96, 97, 98, 99 and 100, and there is no register with address 100.
     * [http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf]
    */
    MEC_IllegalDataAddress = 0x02,

    /**
     * A value contained in the query data field is not an allowable value for 
     * server. This indicates a fault in the structure of the remainder of a 
     * complex request, such as that the implied length is incorrect. It 
     * specifically does NOT mean that a data item submitted for storage in a
     * register has a value outside the expectation of the application program,
     * since the MODBUS protocol is unaware of the significance of any 
     * particular value of any particular register.
     * [http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf]
    */
    MEC_IllegalDataValue = 0x03,

    /**
     * An unrecoverable error occurred while the server was attempting to 
     * perform the requested action.
     * [http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf]
    */
    MEC_ServerDeviceFailure = 0x04,

    /**
     * Specialized use in conjunction with programming commands.
     * The server has accepted the request and is processing it, but a long
     * duration of time will be required to do so. This response is returned to
     * prevent a timeout error from occurring in the client. The client can 
     * next issue a Poll Program Complete message to determine if processing is
     * completed.
     * [http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf] 
    */
    MEC_Acknowledge = 0x05,

    /**
     * Specialized use in conjunction with programming commands.
     * The server is engaged in processing a long–duration program command. 
     * The client should retransmit the message later when the server is free.
     * [http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf] 
    */
    MEC_ServerDeviceBusy = 0x06,
    
    /**
     * Specialized use in conjunction with function codes 20 and 21 and 
     * reference type 6, to indicate that the extended file area failed to pass
     * a consistency check. The server attempted to read record file, but 
     * detected a parity error in the memory. The client can retry the request,
     * but service may be required on the server device.
     * [http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf] 
    */
    MEC_MemoryParityError = 0x08,

    /**
     * Specialized use in conjunction with gateways, indicates that the gateway
     * was unable to allocate an internal communication path from the input 
     * port to the output port for processing the request. Usually means that 
     * the gateway is misconfigured or overloaded.
     * [http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf] 
    */
    MEC_GatewayPathUnavailable = 0x0A,

    /**
     * Specialized use in conjunction with gateways, indicates that no response
     * was obtained from the target device. Usually means that the device is 
     * not present on the network.
     * [http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf] 
    */
    MEC_GatewayTargetDeviceFailedToResponsd = 0x0B


} ModbusExceptionCode_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

/**
 * @brief Modbus Diagnostic Subfunction Codes
 */

typedef enum
{
    /**
     * The data passed in the request data field is to be returned (looped 
     * back) in the response. The entire response message should be identical 
     * to the request.
     * 
     *  Sub-function | Data Field (Request) |       Data Field (Response)
     * --------------|----------------------|----------------------------------
     *     00 00     |        Any           |      Echo Request Data  
     * 
     * [http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf] 
    */ 
    MDS_ReturnQueryData = 0x00,

    /**
     * The remote device serial line port must be initialized and restarted, 
     * and all of its communications event counters are cleared. If the port is
     * currently in Listen Only Mode, no response is returned. This function is
     * the only one that brings the port out of Listen Only Mode. If the port 
     * is not currently in Listen Only Mode, a normal response is returned. 
     * This occurs before the restart is executed.
     * When the remote device receives the request, it attempts a restart and 
     * executes its power–up confidence tests. Successful completion of the 
     * tests will bring the port online.
     * A request data field contents of FF 00 hex causes the port’s 
     * Communications Event Log to be cleared also. Contents of 00 00 leave 
     * the log as it was prior to the restart.
     *
     *  Sub-function | Data Field (Request) |       Data Field (Response)
     * --------------|----------------------|----------------------------------
     *     00 01     |       00 00          |      Echo Request Data  
     *     00 01     |       FF 00          |      Echo Request Data  
     * 
     * [http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf] 
    */ 
    MDS_RestartCommunicationsOptions = 0x01,

    /**
     * The contents of the remote device’s 16–bit diagnostic register are 
     * returned in the response.
     *
     *  Sub-function | Data Field (Request) |       Data Field (Response)
     * --------------|----------------------|----------------------------------
     *     00 02     |       00 00          |   Diagnostic Register Contents  
     * 
     * [http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf] 
    */     
    MDS_ReturnDiagnosticRegister = 0x02,

    /**
     * The character ‘CHAR’ passed in the request data field becomes the end of
     * message delimiter for future messages (replacing the default 
     * LF character). This function is useful in cases of a Line Feed is not 
     * required at the end of ASCII messages.
     *
     *  Sub-function | Data Field (Request) |       Data Field (Response)
     * --------------|----------------------|----------------------------------
     *     00 03     |       CHAR 00        |      Echo Request Data  
     * 
     * [http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf] 
    */     
    MDS_ChangeASCIIInputDelimiter = 0x03,

    /**
     * Forces the addressed remote device to its Listen Only Mode for MODBUS 
     * communications. This isolates it from the other devices on the network,
     * allowing them to continue communicating without interruption from the 
     * addressed remote device. No response is returned.
     * When the remote device enters its Listen Only Mode, all active 
     * communication controls are turned off. The Ready watchdog timer is 
     * allowed to expire, locking the controls off. While the device is in this
     * mode, any MODBUS messages addressed to it or broadcast are monitored, 
     * but no actions will be taken and no responses will be sent. 
     * The only function that will be processed after the mode is entered will 
     * be the Restart Communications Option function (function code 8, 
     * sub-function 1).
     *
     *  Sub-function | Data Field (Request) |       Data Field (Response)
     * --------------|----------------------|----------------------------------
     *     00 04     |       00 00          |       No Response Returned  
     * 
     * [http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf] 
    */       
    MDS_ForceListenOnlyMode = 0x04,

    /**
     * The goal is to clear all counters and the diagnostic register. Counters
     * are also cleared upon power–up.
     *
     *  Sub-function | Data Field (Request) |       Data Field (Response)
     * --------------|----------------------|----------------------------------
     *     00 0A     |       00 00          |       Echo Request Data  
     * 
     * [http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf] 
    */     
    MDS_ClearCountersAndDiagnosticRegister = 0x0A,

    /**
     * The response data field returns the quantity of messages that the remote
     * device has detected on the communications system since its last restart,
     * clear counters operation, or power –up.
     * Exception responses are described and listed in section 7 .
     *
     *  Sub-function | Data Field (Request) |       Data Field (Response)
     * --------------|----------------------|----------------------------------
     *     00 0B     |       00 00          |       Total Message Count  
     * 
     * [http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf] 
    */     
    MDS_ReturnBusMessageCount = 0x0B,

    /**
     * The response data field returns the quantity of MODBUS exception 
     * responses returned by the remote device since its last restart, 
     * clear counters operation, or power–up. 
     * Exception responses are described and listed in section 7 .
     *
     *  Sub-function | Data Field (Request) |       Data Field (Response)
     * --------------|----------------------|----------------------------------
     *     00 0C     |       00 00          |       CRC Error Count  
     * 
     * [http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf] 
    */ 
    MDS_ReturnBusCommunicationErrorCount = 0x0C,

    /**
     * The response data field returns the quantity of MODBUS exception 
     * responses returned by the remote device since its last restart, 
     * clear counters operation, or power–up. 
     * Exception responses are described and listed in section 7 .
     *
     *  Sub-function | Data Field (Request) |       Data Field (Response)
     * --------------|----------------------|----------------------------------
     *     00 0D     |       00 00          |       Exception Error Count  
     * 
     * [http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf] 
    */
    MDS_ReturnBusExceptionErrorCount = 0x0D,

    /**
     * The response data field returns the quantity of messages addressed to 
     * the remote device, or broadcast, that the remote device has processed
     * since its last restart, clear counters operation, or power–up.
     *
     *  Sub-function | Data Field (Request) |       Data Field (Response)
     * --------------|----------------------|----------------------------------
     *     00 0E     |       00 00          |       Server Message Count  
     * 
     * [http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf] 
    */
    MDS_ReturnServerMessageCount = 0x0E,

    /**
     * The response data field returns the quantity of messages addressed to 
     * the remote device for which it has returned no response (neither a 
     * normal response nor an exception response),since its last restart, 
     * clear counters operation, or power–up.
     * 
     *  Sub-function | Data Field (Request) |       Data Field (Response)
     * --------------|----------------------|----------------------------------
     *     00 0F     |       00 00          |     Server No Response Count  
     * 
     * [http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf] 
    */
    MDS_ReturnServerNoResponseCount = 0x0F,

    /**
     * The response data field returns the quantity of messages addressed to 
     * the remote device for which it returned a Negative Acknowledge (NAK) 
     * exception response, since its last restart, clear counters operation, 
     * or power–up. 
     * Exception responses are described and listed in section 7.
     * 
     *  Sub-function | Data Field (Request) |       Data Field (Response)
     * --------------|----------------------|----------------------------------
     *     00 10     |       00 00          |       Server NAK Count  
     * 
     * [http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf] 
    */
    MDS_ReturnServerNAKCount = 0x10,
    
    /**
     * The response data field returns the quantity of messages addressed to 
     * the remote device for which it returned a Server Device Busy exception 
     * response, since its last restart, clear counters operation, or power–up. 
     * 
     *  Sub-function | Data Field (Request) |       Data Field (Response)
     * --------------|----------------------|----------------------------------
     *     00 11     |       00 00          |   Server Device Busy Count  
     * 
     * [http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf] 
    */
    MDS_ReturnServerBusyCount = 0x11,

    /**
     * The response data field returns the quantity of messages addressed to 
     * the remote device that it could not handle due to a character overrun 
     * condition, since its last restart, clear counters operation, or 
     * power–up. A character overrun is caused by data characters arriving at 
     * the port faster than they can be stored, or by the loss of a character 
     * due to a hardware malfunction. 
     * 
     *  Sub-function | Data Field (Request) |       Data Field (Response)
     * --------------|----------------------|----------------------------------
     *     00 12     |       00 00          |   Server Character Overrun Count 
     * 
     * [http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf] 
    */
    MDS_ReturnBusCharacterOverrunCount = 0x12,

    /**
     * Clears the overrun error counter and reset the error flag.  
     * 
     *  Sub-function | Data Field (Request) |       Data Field (Response)
     * --------------|----------------------|----------------------------------
     *     00 14     |       00 00          |       Echo Request Data 
     * 
     * [http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf] 
    */
    MDS_ClearOverrunCounterAndFlag = 0x14


} ModbusDiagnosticsSubfunction_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

/**
 * @brief Modbus Node Role
 */ 

typedef enum 
{
    MNR_Slave = 0,
    MNR_Master = 1,
    MNR_NoSelected

} ModbusNodeRole_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

/**
 * @brief Modbus Frame Data Index
 */ 
/**
 *                      < - - - -         PDU         - - - >
 * | TransmitAddr (1B) | FuncCode (1B) |        Data        | CRC (2B) |
 *                          ....
 *  |                      Data                             |
 *                          ....
 *                        example
 *           | Start Addr (2B) | Quantity  (2B) |
 *                          or
 *           | Start Addr (2B) | Quantity  (2B) |
 * 
 * 
 */

typedef enum 
{
    MFI_TransmitAddr = 0,
    MFI_PDUStart = 1,
    MFI_FuncCode = 1,

    MFI_ExceptionCode = 2,

    MFI_DataAddrHiByte = 2,
    MFI_DataAddrLoByte = 3,

    MFI_DataValHiByte = 4,
    MFI_DataValLoByte = 5,

    MFI_ByteCntByte = 6,
    MFI_OutputsValueBytes = 7,

    MFI_RspByteCnt = 2,
    MFI_RspValuesByte = 3

} ModbusFrameIndex_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

/**
 * @brief Modbus Supported Func States
 */ 

typedef enum 
{
    MES_Illegal = 0,
    MES_Correct = 1

} ModbusExceptionState_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef enum
{
    MIS_ErrNotInitialized,
    MIS_ErrRoleNotSelected,
    MIS_ErrSendDataClbk,
    MIS_ErrFuncDOC,
    MIS_ErrFuncDIC,
    MIS_ErrFuncHReg,
    MIS_ErrFuncIReg,
    MIS_Initialized

} ModbusInitializeStatus_t;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


/**
 * @brief Modbus Supported Func States
 */ 

typedef enum 
{
    MFT_ModbusRequest = 0,
    MFT_ModbusResponse = 1

} ModbusFrameType_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

/**
 * @brief Modbus Supported Func States
 */ 

typedef enum 
{
    MRF_NoData = 0,
    MRF_ReceiveData = 1

} ModbusReceiveFlag_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

/**
 * @brief Modbus Receive Status
 */ 

typedef enum 
{
	MRS_WaitForResponse,
    MRS_ErrorTimeout,
    MRS_ErrorCRC,
    MRS_ErrorFuncCode,
    MRS_ErrorDataSize,
    MRS_ErrorDataValue,
    MRS_CorrectRespond

} ModbusReceiveStatus_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef void (*send_data_callback)(uint8_t*, uint16_t);

/**
 * @brief Modbus Communication structure
 */ 

typedef struct modbus_com_s
{
    const uint8_t* supported_func_a;
    uint8_t supported_func_cnt;

    ModbusReceiveFlag_t receive_flag; 

    uint8_t device_addr;
    ModbusNodeRole_t role;

    uint8_t rec_buff[MODBUS_ADU_SIZE];
    uint16_t rec_buff_size;

    uint8_t adu[MODBUS_ADU_SIZE];
    uint16_t adu_size;

    uint16_t *ir_a;         //input registers
    uint16_t ir_size;       //input registers size

    uint16_t *hr_a;         //holding registers
    uint16_t hr_size;       //holding registers size

    uint8_t *doc_a;         //discrete output coils
    uint16_t doc_size;      //discrete output coils size

    uint8_t *dic_a;         //discrete input contacts
    uint16_t dic_size;      //discrete input contacts size    

    send_data_callback send_data_clbk;

    bool is_init;

} ModbusCom;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


#endif /* __MODBUS_TYPES_H */
