#ifndef __MODBUS_RSP_FUNC_H
#define __MODBUS_RSP_FUNC_H

#include "modbus_types.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void modbus_rsp_read_coils(ModbusCom *mc);
    
void modbus_rsp_read_discrete_inputs(ModbusCom *mc);

void modbus_rsp_read_holding_registers(ModbusCom *mc);

void modbus_rsp_read_input_registers(ModbusCom *mc);

void modbus_rsp_write_single_coil(ModbusCom *mc);

void modbus_rsp_write_single_register(ModbusCom *mc);

void modbus_rsp_write_multiple_coils(ModbusCom *mc);

void modbus_rsp_write_multiple_registers(ModbusCom *mc);

void modbus_rsp_exception(ModbusCom *mc, ModbusFunctionCode_t fc,
     ModbusExceptionCode_t exc_code);


#endif /* __MODBUS_RSP_FUNC_H */